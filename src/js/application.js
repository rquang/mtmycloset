(function ($) {

    //remove clutter
    $("#minor-publishing").fadeOut();
    $("#duplicate-action").fadeOut();
    $("#delete-action").fadeOut();
    $("#post-body-content").fadeOut();

    $("#submitdiv h2 span").html('Publish Changes');

    //wrap acf_form
    // $('#application_item').children().unwrap().wrapAll("<form id='application_form' class='acf-form' action='' method='post' enctype='multipart/form-data'></form>");

    // $(document).trigger('acf/setup_fields', $("#application_form"));
    // acf.do_action('load', $('#application_form'));
    // acf.do_action('ready', $('#application_form'));
    // acf.do_action('append', $('#application_form'));

    // mtUpdateItem($('#application_item'), 76);
    // mtUpdateItem($('#application_item_options'), 223);

    $("#mt_app_item_select").on('change', function (e) {
        if (e.originalEvent) {
            var url = window.location.href,
                separator = (url.indexOf("?") === -1) ? "?" : "&",
                itemID = $("#mt_app_item_select").val(),
                newParam = separator + "post=" + itemID;

            newUrl = url.replace(newParam, "");
            newUrl += newParam;
            window.location.href = newUrl;
            // mtUpdateItem($('#application_item'), 76);
            // mtUpdateItem($('#application_item_options'), 223);
        } else {
            return false;
        }
    });


    // listens to all acf_forms on page.
    // acf.add_filter('validation_complete', function (json, form) {
    //     if (!json.errors) {
    //         //if no errors stop form from being submitted.
    //         form.submit(function (event) {
    //             event.preventDefault();
    //             submitACF_AJAX(this);
    //             return false;
    //         });
    //     }
    //     return json;
    // });

    //sends the request using FormData object will work with file uploads as well.
    // function submitACF_AJAX(form) {
    //     var data = new FormData(form);
    //     $.ajax({
    //             type: 'POST',
    //             url: window.location.href,
    //             data: data,
    //             processData: false,
    //             contentType: false
    //         })
    //         .done(function (data) {
    //             $(form).trigger('acf_submit_complete', data);
    //             console.log('yes');
    //         })
    //         .fail(function (error) {
    //             $(form).trigger('acf_submit_fail', error);
    //             console.log(error);
    //         });
    // }

    function mtUpdateItem($el, field_group) {
        $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {
                action: 'mt_application_item_ajax_request',
                item_id: $("#mt_app_item_select").val(),
                field_group_id: field_group
            },
            beforeSend: function () {
                $el.fadeOut();
            },
            complete: function () {
                $el.fadeIn();
            },
            success: function (data) {
                // This outputs the result of the ajax request
                $el.html(data);
                // acf.do_action('append', $el);
                acf.addAction('append', $("#post"));
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    }
})(jQuery);