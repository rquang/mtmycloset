(function ($) {
    $(document).ready(function () {
        $menu = $("#wcfm_menu");
        $select = $('<select id="wcfm_mt_mobile_menu"></select>');
        $menu.children('.wcfm_menu_items').each(function () {
            var href = $(this).find('a.wcfm_menu_item').attr('href'),
                title = $(this).find('span.text').html().trim(),
                $select_item = '<option value="' + href + '">' + title + '</option>';

            $select.append($select_item);
        });
        $menu.append($select);
        $(document).on('change', "#wcfm_mt_mobile_menu", function (e) {
            if (e.originalEvent) {
                var newUrl = $select.val();
                window.location = newUrl;
            } else {
                return false;
            }
        });
        // $("#mt-delete-item").submit(function (e) {
        //     e.preventDefault();
        //     confirm('Are you sure you want to delete this item?');
        // });
    });

})(jQuery);

function confSubmit(form) {
    if (confirm("Are you sure you want to delete the item?")) {
        form.submit();
    } else {
        return false;
    }
}