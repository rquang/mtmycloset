(function ($) {
    $('#sell_application .acf-repeater .acf-actions .acf-button').last().attr("id", "mt-add-item");
    $('#sell_application .acf-repeater .acf-table tbody').first().attr("id", "mt-items");

    $("#mt-items").children().first().addClass('is--active');

    // TESTING
    // var $field = $('#acf-field_5cc37aea9f477');
    // LIVE
    // var $field = $('#acf-field_5cc37a2abda4e');

    // $(document).on('click', '#mt-add-item', function () {
    //     $field.val('New');
    //     $('#sell_application input[type="submit"]').trigger('click');
    // });
    // $(document).on('click', '#mt-save-application', function () {
    //     $field.val('Update');
    //     $('#sell_application input[type="submit"]').trigger('click');
    // });
    // $(document).on('click', '#mt-send-application', function () {
    //     $field.val('Submit');
    //     $('#sell_application input[type="submit"]').trigger('click');
    // });

})(jQuery);