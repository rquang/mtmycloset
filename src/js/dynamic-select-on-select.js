/*
		updated JS file for use with ACF >= 5.7.0
*/

var type, formal = "No";

jQuery(document).ready(function ($) {
	if (typeof acf == 'undefined') {
		return;
	}
	/*
			In ACF >= 5.7.0 the acf.ajax object no longer exists so we can't extend it
			Instead we need to attach out event the old fashioned way
			and we need to do it using $(document).on because the element may
			be added dynamically and this is the only way to add events
	*/
	// TYPE
	$(document).on('change', '[data-key="field_5c9bfb21157ff"] .acf-input input[type=radio]', function (e) {
		// we are not going to do anyting in this anonymous function
		// the reason is explained below
		// call function, we need to pass the event and jQuery object
		update_category(0, $);
		update_categories_change(e, $, $(this));
	});
	// FORMAL
	$(document).on('change', '[data-key="field_5cf9459d94028"] .acf-input input[type=radio]', function (e) {
		update_category(0, $);
		update_categories_change(e, $, $(this));
	});
	// // CATEGORY
	// $(document).on('change', '[data-key="field_5d0bb3721a822"] .acf-input select', function (e) {
	// 	update_categories_change(e, $, $(this));
	// 	update_category($(this).val(), $);

	// });
	// // SUBCATEGORY
	// $(document).on('change', '[data-key="field_5d0bb3981a823"] .acf-input select', function (e) {
	// 	update_category($(this).val(), $);
	// });
	$('[data-key="field_5c9bfb21157ff"] .acf-input input[type=radio]').trigger('ready');
	init($);
	if (!$('.acf-field-5cf97c1af42c8').hasClass('acf-hidden')) {
		$('#mt-application-footer').fadeIn();
	}
	$(".acf-field-5cf97c1af42c8").bind('classChanged', function () {
		if (!$(this).hasClass('acf-hidden')) {
			$('#mt-application-footer').fadeIn();
		} else {
			$('#mt-application-footer').fadeOut();
		}
	});
	(function (func) {
		$.fn.addClass = function () { // replace the existing function on $.fn
			func.apply(this, arguments); // invoke the original function
			this.trigger('classChanged'); // trigger the custom event
			return this; // retain jQuery chainability
		};
	})($.fn.addClass); // pass the original function as an argument

	(function (func) {
		$.fn.removeClass = function () {
			func.apply(this, arguments);
			this.trigger('classChanged');
			return this;
		};
	})($.fn.removeClass);
});

function init($) {
	var $category = $('.acf-field-5d18c3b0610db'),
		type = $('[data-key="field_5c9bfb21157ff"] .acf-input input[type=radio]').val(),
		formal = $('[data-key="field_5cf9459d94028"] .acf-input input[type=radio]').val();
	switch (type) {
		case "Men's":
			if ($category.hasClass('is--mens'))
				$category.removeClass('is--mens');
			$category.addClass('is--mens').removeClass('is--womens is--bags');
			break;
		case "Women's":
			if ($category.hasClass('is--womens'))
				$category.removeClass('is--womens');
			$category.addClass('is--womens').removeClass('is--mens is--bags');
			break;
		case "Bags":
			if ($category.hasClass('is--bags'))
				$category.removeClass('is--bags');
			$category.addClass('is--bags').removeClass('is--mens is--womens');
			break;
		default:
			break;
	}
	switch (formal) {
		case "Yes":
			$category.addClass('is--formal').removeClass('is--bags');
			break;
		case "No":
			$category.removeClass('is--formal').removeClass('is--bags');
			break;
		default:
			break;
	}
}

// the actual function is separate from the above change function
// the reason for this is that "this" has no real meaning in an anonymous 
// function as each call is a new JS object and we need "this" in order 
// to be to abort previous AJAX requests
function update_categories_change(e, $, $this) {
	var $category = $('.acf-field-5d18c3b0610db');

	if (this.request) {
		// if a recent request has been made abort it
		this.request.abort();
	}

	// get the category/subcategory select field, and remove all exisiting choices
	var cat_select,
		cat_select_1 = $('[data-key="field_5d0bb3721a822"] select'),
		cat_select_2 = $('[data-key="field_5d0bb3981a823"] select');
	if ($this.is('input')) {
		cat_select = cat_select_1;
		cat_select_2.empty();
	} else {
		cat_select = cat_select_2;
	}
	cat_select.empty();

	// get the target of the event and then get the value of that field
	var target = $(e.target);
	var cat_id = target.val();


	switch (cat_id) {
		case "Men's":
			$category.addClass('is--mens').removeClass('is--womens is--bags');
			break;
		case "Women's":
			$category.addClass('is--womens').removeClass('is--mens is--bags');
			break;
		case "Bags":
			$category.addClass('is--bags').removeClass('is--mens is--womens');
			break;
		case "Yes":
			$category.addClass('is--formal').removeClass('is--bags');
			break;
		case "No":
			$category.removeClass('is--formal').removeClass('is--bags');
			break;
		default:
			break;
	}

	// switch (cat_id) {
	// 	case "Men's":
	// 		type = cat_id;
	// 		if (formal == "No") {
	// 			cat_id = 186;
	// 		} else {
	// 			cat_id = 228;
	// 		}
	// 		break;
	// 	case "Women's":
	// 		type = cat_id;
	// 		if (formal == "No") {
	// 			cat_id = 253;
	// 		} else {
	// 			cat_id = 229;
	// 		}
	// 		break;
	// 	case "Bags":
	// 		cat_id = 260;
	// 		break;
	// 	case "Yes":
	// 		formal = cat_id;
	// 		if (type == "Men's") {
	// 			cat_id = 228;
	// 		} else {
	// 			cat_id = 229;
	// 		}
	// 		break;
	// 	case "No":
	// 		formal = cat_id;
	// 		if (type == "Men's") {
	// 			cat_id = 186;
	// 		} else {
	// 			cat_id = 253;
	// 		}
	// 		break;
	// 	default:
	// 		break;
	// }

	// if (!cat_id) {
	// 	// no category selected
	// 	// don't need to do anything else
	// 	return;
	// }

	// // set and prepare data for ajax
	// var data = {
	// 	action: 'load_categories_choices',
	// 	cat_id: cat_id
	// };

	// cat_select.append('<option>Loading...</option>');

	// // call the acf function that will fill in other values
	// // like post_id and the acf nonce
	// data = acf.prepareForAjax(data);

	// // make ajax request
	// // instead of going through the acf.ajax object to make requests like in <5.7
	// // we need to do a lot of the work ourselves, but other than the method that's called
	// // this has not changed much
	// this.request = $.ajax({
	// 	url: acf.get('ajaxurl'), // acf stored value
	// 	data: data,
	// 	type: 'post',
	// 	dataType: 'json',
	// 	success: function (json) {
	// 		if (!json || json.length == 0) {
	// 			$('.acf-field-5d0bb3981a823').addClass('acf-hidden');
	// 			cat_select_2.empty().prop('disabled', 'disabled');
	// 			return;
	// 		}
	// 		cat_select.empty();
	// 		// add the new options to the category field
	// 		for (i = 0; i < json.length; i++) {
	// 			var cat_item = '<option value="' + json[i].value + '">' + json[i].label + '</option>';
	// 			cat_select.append(cat_item);
	// 		}
	// 	}
	// });

}

function update_category(id, $) {
	var $category = $('.acf-field-5d18c3b0610db');
	console.log(id);
	if (id) {
		$category.find('[data-id="' + id + '"]').find('input').first().prop("checked", true).change();
	} else {
		$category.find('input').prop("checked", false);
	}
}