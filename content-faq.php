<div class="row my-5">
<?php 

$args = array(
    'taxonomy' => 'faq_categories',
    'hide_empty' => 1,
);

$terms = get_terms($args);
if ( !empty($terms) ) {
    // populate choices
    foreach( $terms as $term ) {
        ?>
            <div class="col-12 col-sm-6 col-md-4 mb-4">
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="d-block p-4 text-center border">
                    <h4><?php echo $term->name; ?></h4>
                    <span><?php echo $term->description; ?></span>
                </a>
            </div>
        <?php 
    }
}

?>
</div>