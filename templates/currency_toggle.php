<?php
//Exit if accessed directly
if ( !defined('ABSPATH') ) {
    //If wordpress isn't loaded load it up.
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . '/wp-load.php';
}
?>

<ul class="mt-currency-toggle">
    <li><a href="#">CAD &#36;<i class="fa fa-angle-down"></i></a>
        <ul class="sub-menu">
            <li><a href="#">USD - &#36;</a></li>
            <li><a href="#">EUR - &#8364;</a></li>
            <li><a href="#">GBP - &#163;</a></li>
            <li><a href="#">INR - &#8377;</a></li>
        </ul>
    </li>
</ul>