<?php
$settings = array(
    'id' => 'size_chart',
    'post_id' => 'user_' . get_current_user_id(),
    'field_groups' => array(7168),
    'updated_message' => __("Size chart updated", 'acf'),
    // 'return' => add_query_arg( 'updated', 'true', get_permalink() ), 
    'submit_value' => __("Update", 'acf')
    
);
?>
<div class="container py-5">
    <h3>Clothing Size Chart</h3>
    <p>
         As a boutique, we require you to upload Clothing size conversion chart. Clothing size conversion charts give your customer an idea of the right size to buy.
    </p>
    <h4>Examples</h4>
    <div class="row">
        <div class="col-12 col-sm-6">
            <img class="img-fluid" src="https://mtmycloset.com/wp-content/uploads/2019/07/mt-womens-size-chart.jpg" alt="Women's Clothing Size Chart">
        </div>
        <div class="col-12 col-sm-6">
            <img class="img-fluid" src="https://mtmycloset.com/wp-content/uploads/2019/07/mt-mens-size-chart.jpg" alt="Men's Clothing Size Chart">
        </div>
    </div>
    <hr>
</div>
<?php
acf_form($settings);
?>