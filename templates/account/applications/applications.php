<?php 

if($_POST['tracking_number']) :
    update_field('tracking_number', $_POST['tracking_number'], $_POST['app_id']);
endif;

$user_id = get_current_user_id();
$args = array(
    'post_type' => 'application',
    'orderby' => 'publish_date',
    'order' => 'ASC',
    'post_status' => array('publish', 'pending', 'draft', 'auto-draft'),
    'meta_query' => array(
        array(
            'key' => 'user_id',
            'value' => $user_id,
            'compare' => 'LIKE',
        ),
    ),
);

$loop = new WP_Query($args);
?>

<div class="row m-0">
    <div class="col p-3">
        <p>From your applications dashboard you can view the status of your applications and manage existing applications. If you have any questions are concerns, contact us at <a href="mailto:info@mtmycloset.com">info@mtmycloset.com</a> and include your application number.</p>
    </div>
</div>

<table class="mt-table-applications table">
    <thead>
        <tr>
            <th class="text-center">Application #</th>
            <th class="text-center">Total Items</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if ($loop->have_posts()):
            foreach ($loop->get_posts() as $p) :
                // set the item link
                $ID = $p->ID;
                $link = add_query_arg(array(
                    'app_id' => $ID,
                ), home_url($wp->request));
                $status = get_post_status($ID);
                $app_status = get_field('application_status', $ID);
                ?>
                    <tr>
                        <td class="text-center">
                            <?php echo $ID; ?>
                        </td>
                        <td class="text-center">
                            <?php
                                $args2 = array(
                                    'post_type' => 'item',
                                    'orderby' => 'publish_date',
                                    'order' => 'ASC',
                                    'meta_query' => array(
                                        array(
                                            'key' => 'application_id',
                                            'value' => $ID,
                                            'compare' => 'LIKE',
                                        ),
                                    ),
                                );
                                $loop2 = new WP_Query($args2);
                                $item_count = 0;
                                $reject_count = 0;
                                if ($loop2->have_posts()):
                                    foreach ($loop2->get_posts() as $q) :
                                        $item_status = get_field('item_status', $q->ID);
                                        if($item_status == 'Reject') :
                                            ++$reject_count;
                                        endif;
                                        ++$item_count;
                                    endforeach;
                                endif;
                            ?>
                            <?php echo $item_count; ?>
                        </td>
                        <td class='text-capitalize text-center'>
                            <?php 
                            if ($status == 'publish') :
                                echo str_replace("Sent", "Ready For Review", $app_status);
                            elseif ($status == 'draft') :
                                echo $status;
                            endif;
                            ?>
                        </td>
                        <td class='text-capitalize text-center'>
                            <?php if ($app_status == 'Initial Invoice - Sent' || $app_status == 'Final Invoice - Sent' ) : ?>
                                <a href="<?php echo $link; ?>" class="btn btn-secondary">Review</a>
                            <?php elseif ($app_status == 'Shipping Label - Sent') : 
                                $tracking_number = get_field('tracking_number', $ID);
                                $shipping_label_link = get_field('shipping_label', $ID);
                            ?>
                                <a href="<?php echo $shipping_label_link; ?>" target="_blank" class="btn btn-secondary">Print Shipping Label</a>
                                <a href="https://www.ups.com/track?loc=en_US&tracknum=<?php echo $tracking_number; ?>&requester=WT/" target="_blank" class="btn btn-secondary">Track Shipment</a>
                            <?php elseif ($status == 'draft') : ?>
                                <a href="/dashboard/sell/?app_id=<?php echo $ID ?>" class="btn btn-secondary">Edit</a>
                            <?php endif; ?>
                        </td>
                    <tr>
                <?php
            endforeach;
        endif;
        ?>
    <tbody>
</table>
