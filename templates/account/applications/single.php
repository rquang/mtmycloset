<?php

$app_id = $_GET['app_id'];
$app_status = get_field('application_status', $app_id);
$app_notes = get_field('application_notes', $app_id);

?>

<div class="row m-0 pt-3">
    <div class="col-12 col-md-6">
        <h2 class="d-block float-none">Application #<?php echo $app_id; ?></h2>
        <h5 class="d-block float-none">
            <?php if( $app_status == 'Initial Invoice - Sent') : ?>
            Initial Evaluation
            <?php else : ?>
            Final Evaluation
            <?php endif; ?>
        </h5>
    </div>
    <div class="col-12 col-md-6">
        <a href="<?php echo home_url($wp->request); ?>" class="btn btn-secondary float-right">All Applications</a>
    </div>
    <?php if( $app_notes ) : ?>
    <div class="col-12">
        <hr>
        <h6><strong>Application Notes:</strong></h6>
        <p>
            <?php echo $app_notes; ?>
        </p>
    </div>
    <?php endif; ?>
</div>
<form action="<?php echo home_url($wp->request); ?>" method="post">
    <input type="hidden" name="application_id" value="<?php echo $app_id; ?>">
    <table class="mt-table-current-application table table-responsive-lg">
        <thead>
            <th class="text-center">Image</th>
            <th class="text-center">Information</th>
            <th class="text-center">Condition (1-5)</th>
            <th class="text-center">Listing Price</th>
            <th class="text-center">Commission</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            <?php 
                $args = array(
                    'post_type' => 'item',
                    'orderby' => 'publish_date',
                    'order' => 'ASC',
                    'meta_query' => array(
                        array(
                            'key' => 'application_id',
                            'value' => $app_id,
                            'compare' => 'LIKE',
                        ),
                    ),
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    $count = 0;
                    $reject_count = 0;
                    $total = 0;
                    $earnings = 0;
                    foreach ($loop->get_posts() as $p) :
                        $ID = $p->ID;
                        $type = get_field('type', $ID)['type'];
                        $formal = get_field('type', $ID)['formal'];
                        $image = get_field('photos_clothing', $ID)['front'];
                        // switch ([$type, $formal]) {
                        //     case ["Men's",'No']:
                        //         $category = get_field('type', $ID)['mens_categories']->name;
                        //         $size = get_field('information', $ID)['mens_size']->name;
                        //         break;
                        //     case ["Women's",'No']:
                        //         $category = get_field('type', $ID)['mens_categories']->name;
                        //         $size = get_field('information', $ID)['womens_size']->name;
                        //         break;
                        //     case ["Men's",'No']:
                        //         $category = get_field('type', $ID)['formal_mens_categories']->name;
                        //         $size = get_field('information', $ID)['formal_mens_size']->name;
                        //         break;
                        //     case ["Women's",'No']:
                        //         $category = get_field('type', $ID)['formal_womens_categories']->name;
                        //         $size = get_field('information', $ID)['formal_womens_size']->name;
                        //         break;
                        //     default: 
                        //         $image = get_field('photos_bag', $ID)['front'];
                        //         $category = get_field('type', $ID)['bags_categories']->name;
                        //         $size = "OS";
                        //         break;               
                        // }             
                        $category = get_field('type', $ID)['category']->name;
                        $size = get_field('information', $ID)['size'];
                        $name = get_field('product_name', $ID)['product_name'];
                        $designer = get_field('information', $ID)['designer']->name;
                        $wear_tear = get_field('condition', $ID)['wear_&_tear'];
                        $embellishments = get_field('condition', $ID)['missing_embellishments'];
                        $stains = get_field('condition', $ID)['stains'];
                        $retail_price = number_format((float)get_field('pricing', $ID)['retail_price'], 2, '.', '');
                        $listing_price = number_format((float)get_field('pricing', $ID)['listing_price'], 2, '.', '');
                        $suggested_price = number_format((float)get_field('suggested_price', $ID), 2, '.', '');
                        $commission = get_field('commission', $ID);
                        $item_status = get_field('item_status', $ID);
                        if($item_status == 'Approved') :
                            $total += $listing_price;
                            $earnings += $listing_price * ($commission/100);
                        elseif ($item_status == 'Rejected') :
                            ++$reject_count;
                        endif;
                        ++$count;
                        ?>
            <tr>
                <td>
                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </td>
                <td>
                    <strong><?php echo $name; ?></strong>
                    <ul>
                        <li class="py-0"><small><?php echo $type; ?></small></li>
                        <li class="py-0"><small><?php echo $category; ?></small></li>
                        <li class="py-0"><small><?php echo $designer; ?></small></li>
                        <li class="py-0"><small><?php echo $size; ?></small></li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><small>Wear & Tear: <strong><?php echo $wear_tear; ?></strong></small></li>
                        <li><small>Missing Embellishments: <strong><?php echo $embellishments; ?></small></strong></li>
                        <li><small>Stains: <strong><?php echo $stains; ?></strong></small></li>
                    </ul>
                </td>
                <td class="text-center">
                    <?php if ( $suggested_price > 0) : ?>
                        <span><strike>$<?php echo $listing_price; ?> </strike><strong>$<?php echo $suggested_price; ?></strong></span>
                    <?php else : ?>
                        <strong>$<?php echo $listing_price; ?></strong>
                    <?php endif; ?>
                </td>
                <td class="text-center">
                    <?php if($item_status == 'Approved') : ?>
                    <strong class="text-success"><?php echo $commission; ?>%</strong>
                    <?php else : ?>
                    <strong class="text-uppercase text-danger">Rejected</strong>
                    <?php endif; ?>
                </td>
                <td class="text-center">
                    <?php if($item_status == 'Approved') : ?>
                    <select name="<?php echo $ID; ?>">
                        <option value="Approved">Approve Item</option>
                        <?php if( $app_status == 'Initial Invoice - Sent') : ?>
                        <option value="Rejected">Reject Item</option>
                        <?php elseif ( $app_status == 'Final Invoice - Sent') : ?>
                        <option value="Return">Return Item</option>
                        <option value="Donate">Donate Item</option>
                        <?php endif; ?>
                        <?php if ( $suggested_price > 0) : ?>
                            <option value="Counter">Counter Listing Price</option>
                        <?php endif; ?>
                    </select>
                    <?php endif; ?>
                </td>
            </tr>
            <?php
                    endforeach;
                endif;
            ?>
        </tbody>
    </table>
    <?php
    $total = number_format((float)$total, 2, '.', '');
    $earnings = number_format((float)$earnings, 2, '.', '');
    ?>
    <div class="row m-0 justify-content-end">
        <div class="col-12 col-lg-6 col-xl-7">
            <h6>Disclaimer:</h6>
            <ul>
                <?php if( $app_status == 'Initial Invoice - Sent') : ?>
                <li>
                    This is an initial evaluation. Commission rates and possible earnings for each item are subject to
                    review. All items will be thoroughly inspected once received and a final evaluation will be sent for
                    approval.
                </li>
                <?php elseif ( $app_status == 'Final Invoice - Sent') : ?>
                <li>
                    Seller agrees to pay all shipping charges accrued on any item that is marked for return within this
                    application.
                </li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="col-12 col-lg-6 col-xl-5">
            <table class="h5">
                <tr>
                    <td class="text-right">Total Listing Price:</td>
                    <td class="text-right"><strong>$<?php echo $total; ?></strong></td>
                </tr>
                <tr>
                    <td class="text-right">Total Possible Earnings:</td>
                    <td class="text-right text-success"><strong>$<?php echo $earnings; ?></strong></td>
                </tr>
                <?php if($count != $reject_count) : ?>
                <tr>
                    <td colspan="2">
                        <button type="submit" class="btn btn-primary btn-lg w-100">Submit Application</button>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
</form>