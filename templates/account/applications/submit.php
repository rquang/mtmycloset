<?php

$count = 0;
$reject_count = 0;

$user_id = get_current_user_id();

// Get all POST data
foreach ($_POST as $key => $value) :
    if ($key == 'application_id') :
        $app_id = $value;
    else :
        ++$count;
        if ($value == 'Reject')
            ++$reject_count;
        update_field('item_status', $value, $key);
    endif;
endforeach;

if ($count == $reject_count)
    update_field('application_status', 'Rejected', $app_id);
else {
    $app_status = get_field('application_status', $app_id);

    // Update Application Status accordingly
    switch ($app_status) {
        case 'Initial Invoice - Sent' :
            update_field('application_status', 'Initial Invoice - Reviewed', $app_id);
            mt_email_reviewed_application_initial($user_id, $app_id);
            break;
        case 'Final Invoice - Sent' :
            update_field('application_status', 'Final Invoice - Reviewed', $app_id);
            mt_email_reviewed_application_final($user_id, $app_id);
            break; 
    }
}

?>

<div class="row m-0">
    <div class="col py-3">
        <h2 class="float-none d-block">Application #<?php echo $app_id ?> - Submitted for Review</h2>
        <h6 class="float-none d-block">
            Thank you for reviewing your application. We will notify you once we've processed the application.
        </h6>
        <a href="/dashboard/application/" class="btn btn-primary btn-lg mt-3">All Applications</a>
    </div>
</div>