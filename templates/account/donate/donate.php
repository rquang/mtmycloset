<?php
$settings = array(
    'id' => 'donation',
    'post_id' => 'user_' . wp_get_current_user()->ID,
    'field_groups' => array(4699),
    'submit_value' => __("Update", 'acf'),
    'updated_message' => __("Donation Settings Updated", 'acf'),

);

acf_form($settings);
