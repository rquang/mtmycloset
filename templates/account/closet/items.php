<?php
global $product, $woocommerce, $woocommerce_loop;

$closet_id = mt_get_closet_id(get_current_user_id());
$loop = mt_get_closet_items($closet_id);
?>
<div class="row">
    <div class="col">
        <p>The list of all your active/inactive listings in your closet.</p>
    </div>
</div>
<table class="mt-table-items table table-responsive-lg">
    <thead>
        <tr class="text-center">
            <th>Image</th>
            <th>Information</th>
            <th>Favorited</th>
            <th>List Date</th>
            <th>Price</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php
        if ($loop->have_posts()):
            foreach ($loop->get_posts() as $p):
                $ID = $p->ID;
                $product = wc_get_product($ID);
                $regular_price = number_format((float) $product->get_regular_price(), 2, '.', '');
                $sale_price = number_format((float) $product->get_sale_price(), 2, '.', '');
                $start_date = $product->get_date_created()->format('Y-m-d');
                $end_date = current_time('Y-m-d');
                $date_diff = floor((strtotime($start_date) - strtotime($end_date)) / 3600 / 24 * -1);
                $likes = mt_get_total_likes($ID);
                $designer = get_field('designer', $ID)->name;
                $size = get_field('size', $ID)->name;
                $categories = wc_get_product_category_list($ID);
                $status = get_field('product_status', $ID);
                ?>
                <tr class="text-center">
                    <td><?php echo $product->get_image(); ?></td>
                    <td>
                        <?php if ($status == 'Listed') : ?>
                            <a href="<?php the_permalink($ID); ?>"><?php echo $product->get_name(); ?></a>
                        <?php else : ?>
                            <?php echo $product->get_name(); ?>
                        <?php endif; ?>
                        <ul>
                            <li class="py-0"><small><?php echo $categories; ?></small></li>
                            <li class="py-0"><small><?php echo $designer; ?></small></li>
                            <li class="py-0"><small><?php echo $size; ?></small></li>
                        </ul>
                    </td>
                    <td><?php echo $likes; ?></td>
                    <td>
                        <?php echo $start_date; ?>
                        <br>
                        <?php echo $date_diff; ?> days ago
                    </td>
                    <td>
                        <?php 
                        if( $product->is_on_sale() ) :
                            echo "$" . $sale_price;
                        else :
                            echo "$" . $regular_price; 
                        endif; 
                        ?>
                    </td>
                    <td>
                        <?php if ($status == 'Listed') : ?>
                            <form action="<?php echo get_stylesheet_directory_uri(); ?>/inc/item.php" method="post" class="d-flex flex-column" onSubmit="return confirm('Are you sure you want to perform the action?')">
                                <select name="<?php echo $ID; ?>">
                                    <option value="0">Choose an Option</option>
                                    <?php if ($sale_price > 150.00 || ( $regular_price > 150.00 && $sale_price == 0 ) ) : ?>
                                        <option value="1">Lower Price by 10%</option>
                                    <?php endif; ?>
                                    <option value="2">Promote</option>
                                    <option value="3">Return</option>
                                    <option value="4">Donate</option>
                                </select>
                                <button type="submit" class="btn btn-secondary mt-1">Submit</button>
                            </form>
                        <?php else : ?>
                            <b><?php echo $status; ?></b>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php
            endforeach;
        endif;
        ?>
    </tbody>
</table>
