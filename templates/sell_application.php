<?php
global $wp;
$count = 0;

if (isset($_GET['app_id'])) {
    $application_id = $_GET['app_id'];
}
if (isset($_GET['item_id'])) {
    $item_id = $_GET['item_id'];
} else {
    $item_id = 'new_item';
}

$user_id = get_current_user_id();
$user = get_user_meta($user_id);
// $user_data = get_userdata( $user_id);
// write_log($user);
// write_log($user_data);

$args = array(
    'post_type' => 'application',
    'orderby' => 'publish_date',
    'order' => 'ASC',
    'post_status' => array('publish', 'pending'),
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'user_id',
            'value' => $user_id,
            'compare' => 'LIKE',
        ),
        array(
            'key' => 'application_status',
            'value' => 'Complete',
            'compare' => '!=',
        ),
        array(
            'key' => 'application_status',
            'value' => 'Rejected',
            'compare' => '!=',
        ),
    ),
);
$loop = new WP_Query($args);
$count = $loop->found_posts;

if ($count == 0):
    if (isset($_GET['app_id'])) :
        // QUERY PRODUCTS
        $user_id = get_current_user_id();

        if (get_field('user_id', $application_id) != $user_id) {
            // get_wcfm_url();
            mt_redirect('/dashboard/application');
        }

        $args = array(
            'post_type' => 'item',
            'orderby' => 'publish_date',
            'order' => 'ASC',
            'meta_query' => array(
                array(
                    'key' => 'application_id',
                    'value' => $application_id,
                    'compare' => 'LIKE',
                ),
            ),
        );
        $loop = new WP_Query($args);
        if ($loop->have_posts()):
        ?>
            <div id='mt_app_item'>
                <h6>Item Number</h6>
                <ul id='mt_app_item_user_select'>
                    <?php foreach ($loop->get_posts() as $p) :
                            // set the item link
                            $link = add_query_arg(array(
                                'app_id' => $application_id,
                                'item_id' => $p->ID,
                            ), home_url($wp->request));
                            // update the count
                            ++$count;?>
                            <li class='mt_app_item_user_select_item <?php if ($item_id == $p->ID): ?>is--active<?php endif;?>'>
                                <a href="<?php echo $link; ?>"><?php echo $count; ?></a>
                            </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif;
    endif; ?>

    <form id="sell_application" class="acf-form" action="" method="post" enctype="multipart/form-data">
        <?php
        $settings = array(
            'id' => 'sell_application',
            'post_id' => $item_id,
            // LIVE
            // 'field_groups' => array(320),
            // TEST
            'field_groups' => array(72),
            // 'uploader' => 'basic',
            'form' => false,
        );

        acf_form($settings);
        ?>

        <div id="mt-application-footer" class="container text-center my-3 pt-5 border-top">
            <div class="row">
                <div class="col">
                    <p>
                        MT MyCloset will provide you with a shipping label once the review of the items are completed and the
                        initial quote has been agreed to according to our selling policy. Your items pictures will be displayed
                        as "coming soon" once the final quote is approved.
                    </p>
                    <p>
                        Once we receive the items, we will complete our final approval and valuation of your items. You will
                        receive a final quote. All listing prices are subject to review by MT MyCloset. You will be notified
                        once your items have been loaded into your closet.
                    </p>
                </div>
            </div>
            <div class="row my-3">
                <?php if ($count < 10): ?>
                    <div class="col-12 col-md-6 col-lg-4 mb-1">
                        <input type="submit" name="submit_add" value="Add Another Item" id="mt-add-item" class="w-100 btn outline-btn">
                    </div>
                <?php endif;?>
                    <!-- <?php if ($count >= 1): ?>
                        <div class="col-12 col-md-6 col-lg-3 mb-1">
                            <input type="submit" name="submit_delete" value="Delete Item" id="mt-delete-item" class="w-100 btn secondary-btn" onClick="confSubmit(this.form);">
                        </div>
                    <?php endif;?> -->
                <div class="col-12 col-md-6 col-lg-4 mb-1">
                    <input type="submit" name="submit_save" value="Save Draft" id="mt-save-application" class="w-100 btn secondary-btn">
                </div>
                <div class="col-12 col-md-6 col-lg-4 mb-1">
                    <input type="submit" name="submit_send" value="Send Application" id="mt-send-application" class="w-100 btn primary-btn">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p>
                        By clicking "Submit Application" I am agreeing that this listing adheres to the <a href="/terms-agreements">Terms & Agreements</a>.
                    </p>
                </div>
            </div>
        </div>
    </form>
<?php else: ?>
    <div class="container py-5">
        <h3>Application In Progress</h3>
        <p>
            You currently have a application in progress. We only allow closets to have one active application at a time. Once we've completed your current application, you may make another submission.
        </p>
        <p>
            Thank You,
            <br>
            <b>MT MyCloset Team</b>
        </p>
    </div>
<?php endif;?>

