<?php
$settings = array(
    'id' => 'influencer_application',
    'post_id' => 'new_influencer',
    'field_groups' => array(5715),
    'submit_value' => __("Submit Application", 'acf'),
    'fields' => array('field_5d1d1897eec00', 'field_5d1d25ab9d2d2'),
);
?>

<div class="container py-5">
    <h3>Register to Join Our Influencers Roster Today!</h3>
    <p>
        We are here to support you and make sure you are fairly compensated for your work and creativity. We pay very
        close attention to our influencers’ unique voices to make sure to only present you with opportunities that are
        compatible with your personal brand and your followers expectations.
    </p>
    <h4 class="mb-5">
        In order to qualify to join as an MT MyCloset Influencer, the following critera must be met.
    </h4>
    <div class="row mb-5">
        <div class="col-12 col-md-6">
            <h3>Qualifications</h3>
            <ul>
                <li>Your account must be a public profile (Not private)</li>
                <li>5000 Followers Minimum</li>
                <li>6% Engagement rate Minimum</li>
            </ul>
        </div>
        <div class="col-12 col-md-6">
            <h3>Requirements</h3>
            <ul>
                <li>
                    <strong>Minimum of 2 social media posts on Instagram</strong>
                    <ul class="pl-5 mt-2">
                        <li>Explain benefits of MT MyCloset</li>
                        <li>Thoughts around it</li>
                        <li>Tag MT MyCloset on each post</li>
                    </ul>
                </li>
                <li>Must follow #mtmycloset</li>
                <li>Must use #profitinyourpocket and #buysmarter</li>
            </ul>
        </div>
    </div>
    <hr>
</div>

<?php
acf_form($settings);

?>