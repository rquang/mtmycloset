<h2>Men's Sizing</h2>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tops
        (Includes: Formal Indian Wear, Jackets, Suit Jackets, Shirts, etc.)<span
            class="Apple-converted-space"> </span></span><em
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><br
                style="box-sizing: border-box;" /></u></em></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Chest
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                42 - 44</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                46 - 48</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                54 - 56</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span></p>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Pants
        (Includes: Dress Pants, Chinos, Jeans, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Waist
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                25 - 28</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 35</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                36 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; vertical-align: baseline; font-weight: inherit !important; font-size: inherit !important; line-height: inherit !important; font-family: inherit !important; margin: 0px 0px inherit !important 0px;">42 +</span><u
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"></u>
            </td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span><br style="box-sizing: border-box;" /><br
        style="box-sizing: border-box;" /></p>
