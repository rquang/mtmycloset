<h2>Women's Sizing</h2>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tops
        (Includes: Formal Indian Wear, Shirts, Dresses, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 34</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35 - 37</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41 - 43</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                44 - 46</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                47 - 49</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span><span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><br
            style="box-sizing: border-box;" /></span></p>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Casual
        Wear (Includes: Dresses, Skirts, Pants, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 34</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35 - 37</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41 - 43</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                44 - 46</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                47 - 49</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">US</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (in)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                0</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                31.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                4</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                33.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                6</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                8</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                37.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                10</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                39.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                12</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                14</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                43.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                4 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                16</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                46.5</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note:
                Sizes may vary depending on the brand of the product</u></em></span></p>
