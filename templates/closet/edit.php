<?php
$user = wp_get_current_user();
$ID = mt_get_closet_id($user->ID);
$settings = array(
    'id' => 'edit_closet',
    'post_id' => $ID,
    'field_groups' => array(180),
    'fields' => array(
        'field_5ca4fc16ee7ed',
        'field_5ca4fc3cee7ee',
        'field_5ca4fc9dee7f2'
    ),
    'uploader' => 'basic',
    'return' => home_url($wp->request),

);

acf_form($settings);

?>