<?php
$ID = get_the_ID();
$user = get_field('user_id', $ID);
$p_img = get_field('closet_profile_picture', $ID);
$bg_img = get_field('closet_cover_picture', $ID);
$desc = get_field('closet_description', $ID);
$item_count = mt_get_total_closet_items($ID);
$sale_count = mt_get_total_closet_items_sold($ID);
$followers_count = mt_get_total_closet_followers($ID);
$following_count = mt_get_total_closet_following($ID);
?>

<a href="<?php the_permalink(); ?>" class="mt-closet-item">
    <div class="row">
        <div class="col-12">
            <?php if ($bg_img) : ?>
                <img class="mt-closet-bg" src="<?php echo $bg_img['url']; ?>" alt="<?php echo $bg_img['alt']; ?>" />
            <?php else : ?>
                <img class="mt-closet-bg" src="https://via.placeholder.com/1920x400?text=Cover+Photo"/>
            <?php endif; ?>
        </div>
    </div>

    <div class="row mx-0">
        <div class="col-12 col-md-3 col-lg-2 border-left border-bottom">
            <?php if ($bg_img) : ?>
                <img class="mt-closet-avatar border" src="<?php echo $p_img['url']; ?>" alt="<?php echo $p_img['alt']; ?>" />
            <?php else : ?>
                <img class="mt-closet-avatar border" src="https://via.placeholder.com/300x300/000000/FFFFFF?text=Avatar"/>
            <?php endif; ?>
        </div>
        <div class="col-12 col-md-9 col-lg-10 border-right border-bottom">
            <div class="row h-100">
                <div class="col-12 col-lg-5 d-flex align-items-center">
                    <h3 class="d-inline-block mb-0"><?php echo $user['display_name']; ?></h3>
                    <?php if (get_field('closet_influencer', $ID)) : ?>
                        <span class="mt_influencer ml-2"></span>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-lg-7">
                    <div class="row h-100 align-items-center">
                        <div class="col text-center">
                            <span class="h3 d-block meta-count"><?php echo $item_count; ?></span>
                            <span class="meta-title">Items</span>
                        </div>
                        <div class="col text-center">
                            <span class="h3 d-block meta-count"><?php echo $sale_count; ?></span>
                            <span class="meta-title">Sales</span>
                        </div>
                        <div class="col text-center">
                            <span class="h3 d-block meta-count"><?php echo $followers_count; ?></span>
                            <span class="meta-title">Followers</span>
                        </div>
                        <div class="col text-center">
                            <span class="h3 d-block meta-count"><?php echo $following_count; ?></span>
                            <span class="meta-title">Following</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</a>