<?php
$closet_id = get_the_ID();
$loop = mt_get_closet_items($closet_id, 4);
?>
<div class="fixed box box-common">
    <div class="box-product-list">
        <div class="box-products woocommerce columns-4">
            <?php
            if ( $loop->have_posts() ) {
                while ( $loop->have_posts() ) : $loop->the_post();
                    wc_get_template_part( 'content', 'product' );
                endwhile;
            }
            ?>
        </div>
    </div>
</div>