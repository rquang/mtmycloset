<?php

add_filter( 'woocommerce_get_endpoint_url', 'mt_redirect_endpoint', 10, 4 );
function mt_redirect_endpoint ($url, $endpoint, $value, $permalink)
{
    if( $endpoint == 'downloads')
        $url = '/dashboard/withdrawal/';
    return $url;
}

// add_filter("woocommerce_get_query_vars", function ($vars) {
//     foreach (["applications", "sell"] as $e) {
//             $vars[$e] = $e;
//     }
//     return $vars;
// });

// function mt_account_endpoint() {
//     add_rewrite_endpoint( 'applications', EP_ROOT | EP_PAGES );
//     add_rewrite_endpoint( 'sell', EP_ROOT | EP_PAGES );
// }

// add_action( 'init', 'mt_account_endpoint' );

// add_filter( 'woocommerce_account_menu_items', 'mt_new_menu_items' );
//  /**
// * Insert the new endpoint into the My Account menu.
// *
// * @param array $items
// * @return array
// */
// function mt_new_menu_items( $items ) {
//     $items[ 'applications' ] = __( 'Applications', 'applications' );
//     $items[ 'sell' ] = __( 'Sell Items', 'sell' );
//     return $items;
// }

// add_action( 'woocommerce_account_sell_endpoint', 'mt_sell_endpoint_content' );
// function mt_sell_endpoint_content() {
//     global $wp;
//     get_template_part('/templates/sell_application');

// }

// add_action( 'woocommerce_account_applications_endpoint', 'mt_applications_endpoint_content' );
// function mt_applications_endpoint_content() {
//     ob_start();

//     global $wp;

//     if (!empty($_POST)) :
//             get_template_part('/templates/account/applications/submit');
//     elseif (!isset($_GET['app_id'])) :
//             get_template_part('/templates/account/applications/applications');
//     else :
//             get_template_part('/templates/account/applications/single');
//     endif;
// }

// Remove account endpoints
add_filter('woocommerce_account_menu_items', 'mt_remove_my_account_links');
function mt_remove_my_account_links($menu_links)
{

    // unset( $menu_links['edit-address'] ); // Addresses
    unset($menu_links['dashboard']); // Remove Dashboard
    //unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    //unset( $menu_links['orders'] ); // Remove Orders
    // unset($menu_links['downloads']); // Disable Downloads
    //unset( $menu_links['edit-account'] ); // Remove Account details tab
    //unset( $menu_links['customer-logout'] ); // Remove Logout link

    return $menu_links;

}

// Reorder endpoints
function mt_my_account_order() {
    $myorder = array(
        // 'dashboard'          => __( 'Dashboard', 'woocommerce' ),
        'orders'             => __( 'Orders', 'woocommerce' ),
        'downloads'       => __( 'Withdrawals', 'woocommerce' ),
        'edit-address'       => __( 'Addresses', 'woocommerce' ),
        'edit-account'       => __( 'Account Details', 'woocommerce' ),
        'payment-methods'    => __( 'Payment Methods', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),
    );
    return $myorder;
}
add_filter ( 'woocommerce_account_menu_items', 'mt_my_account_order' );

// Redirect if an Application is already reviewed
add_action('wp_loaded', 'my_custom_redirect');
function my_custom_redirect()
{
    if (isset($_GET['app_id'])) {
        $app_id = $_GET['app_id'];
        $app_status = get_field('application_status', $app_id);
        if (strpos($app_status, 'Reviewed') !== false):
            wp_redirect(home_url($wp->request), 301);
            exit;
        endif;
    }
}

/**
 * Remove password strength check.
 */
function iconic_remove_password_strength()
{
    wp_dequeue_script('wc-password-strength-meter');
}
add_action('wp_print_scripts', 'iconic_remove_password_strength', 10);

function wc_social_login_email_link_additional_roles($roles)
{
    $roles = [];
    $roles[] = 'wcfm_vendor';
    return $roles;
}
add_filter('wc_social_login_find_by_email_allowed_user_roles', 'wc_social_login_email_link_additional_roles');
