<?php
//making the meta box (Note: meta box != custom meta field)
function wpse_add_mt_app_meta_box()
{
    add_meta_box(
        'mt_app_meta_box_4', // $id
        'Item List', // $title
        'show_mt_app_meta_box_4', // $callback
        'application', // $page
        'normal', // $context
        'high', // $priority
        'side'
    );
    add_meta_box(
        'mt_app_meta_box_1', // $id
        'Item Number', // $title
        'show_mt_app_meta_box_1', // $callback
        'item', // $page
        'normal', // $context
        'high', // $priority
        'side'
    );
    add_meta_box(
        'mt_app_address', // $id
        'Closet Address', // $title
        'show_mt_app_address', // $callback
        'application', // $page
        'normal', // $context
        'high', // $priority
        'side'
    );
}

add_action('add_meta_boxes', 'wpse_add_mt_app_meta_box');

//showing custom form fields
function show_mt_app_meta_box_1()
{
    global $post;

    wp_nonce_field(basename(__FILE__), 'show_mt_app_meta_box_1');

    if ($post->post_type == 'application') {
        $application_id = $post->ID;
    } else {
        $application_id = get_field('application_id');
    }

    // QUERY PRODUCTS
    $args = array(
        'post_type' => 'item',
        'orderby' => 'publish_date',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'application_id', // name of custom field
                'value' => $application_id, // matches exactly "123", not just 123. This prevents a match for "1234"
                'compare' => 'LIKE',
            ),
        ),
    );
    $loop = new WP_Query($args);
    $count = 0;
    echo "<select id='mt_app_item_select'>";
    echo "<option value='" . $application_id . "'>Application Options</option>";
    if ($loop->have_posts()):
        foreach ($loop->get_posts() as $p) {
            ++$count;
            if ($post->ID == $p->ID) {
                echo "<option value='" . $p->ID . "' selected>" . $count . "</option>";
            } else {
                echo "<option value='" . $p->ID . "'>" . $count . "</option>";
            }
        }
    endif;
    echo "</select>";
    // wp_reset_query();
}
function show_mt_app_meta_box_2()
{
    global $post;

    wp_nonce_field(basename(__FILE__), 'show_mt_app_meta_box_2');

    // if (isset($_GET['item_id'])) {
    //     $item_ID = $_GET['item_id'];
    // } else {
    //     $args = array(
    //         'post_type' => 'item',
    //         'orderby' => 'publish_date',
    //         'order' => 'ASC',
    //         'meta_query' => array(
    //             array(
    //                 'key' => 'application_id', // name of custom field
    //                 'value' => $post->ID, // matches exactly "123", not just 123. This prevents a match for "1234"
    //                 'compare' => 'LIKE',
    //             ),
    //         ),
    //     );
    //     $loop = new WP_Query($args);
    //     if ($loop->have_posts()) {
    //         foreach ($loop->get_posts() as $p) {
    //             $item_ID = $p->ID;
    //         }
    //     }
    // }
    // wp_reset_query();

    echo ('<div id="application_item"></div>');
}
function show_mt_app_meta_box_3()
{
    global $post;

    wp_nonce_field(basename(__FILE__), 'show_mt_app_meta_box_3');

    echo ('<div id="application_item_options"></div>');
}
function show_mt_app_meta_box_4()
{
    global $post;

    wp_nonce_field(basename(__FILE__), 'show_mt_app_meta_box_4');
    if ($post->post_type == 'application') {
        $application_id = $post->ID;
    } else {
        $application_id = get_field('application_id');
    }

    // QUERY PRODUCTS
    $args = array(
        'post_type' => 'item',
        'orderby' => 'publish_date',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'application_id', // name of custom field
                'value' => $application_id, // matches exactly "123", not just 123. This prevents a match for "1234"
                'compare' => 'LIKE',
            ),
        ),
    );
    $loop = new WP_Query($args);
    $count = 0;
    ?>
    <table id='mt_app_admin_table' style="width: 100%; text-align: center;">
        <thead>
            <th>Item #</th>
            <th>Name</th>
            <th>Price</th>
            <th>Commission</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
            <?php
            if ($loop->have_posts()):
                foreach ($loop->get_posts() as $p) :
                    $ID = $p->ID;
                    ++$count;
                    $name = get_field('product_name', $ID)['product_name'];
                    $retail_price = number_format((float)get_field('pricing', $ID)['retail_price'], 2, '.', '');
                    $listing_price = number_format((float)get_field('pricing', $ID)['listing_price'], 2, '.', '');
                    $suggested_price = number_format(get_field('suggested_price', $ID), 2, '.', '');
                    $commission = get_field('commission', $ID);
                    $item_status = get_field('item_status', $ID); 
                ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $name; ?></td>
                        <td>
                            <ul>
                                <li>R: $<?php echo $retail_price; ?></li>
                                <li>L: $<?php echo $listing_price; ?></li>
                                <li>S: $<?php echo $suggested_price; ?></li>
                            </ul>
                        </td>
                        <td><?php echo $commission; ?>%</td>
                        <td><?php echo $item_status; ?></td>
                        <td><a href="<?php echo get_edit_post_link($ID); ?>">View</a></td>
                    </tr>
                <?php
                endforeach;
            endif;
            ?>
        </tbody>
    </table>
    <?php
}
function show_mt_app_address()
{
    global $post;

    wp_nonce_field(basename(__FILE__), 'show_mt_app_address');

    if ($post->post_type == 'application') {
        $application_id = $post->ID;
    } else {
        $application_id = get_field('application_id');
    }
    $user_id = get_field('user_id');
    $user = get_userdata($user_id);

    ?>
    <table id='mt_app_admin_table'>
        <tbody>
            <tr>
                <td><b>Full Name</b></td>
                <td><?php echo $user->user_firstname . ' ' . $user->user_lastname;?></td>
            </tr>
            <tr>
                <td><b>Email</b></td>
                <td><?php echo $user->user_email; ?></td>
            </tr>
            <tr>
                <td><b>Street</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_street_1')[0] ?></td>
            </tr>
            <tr>
                <td><b>Street 2</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_street_2')[0] ?></td>
            </tr>
            <tr>
                <td><b>City</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_city')[0] ?></td>
            </tr>
            <tr>
                <td><b>Zip/Postal Code</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_zip')[0] ?></td>
            </tr>
            <tr>
                <td><b>State/Province</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_state')[0] ?></td>
            </tr>
            <tr>
                <td><b>Country</b></td>
                <td><?php echo get_user_meta($user_id,'_wcfm_country')[0] ?></td>
            </tr>
        </tbody>
    </table>
    <?php
}