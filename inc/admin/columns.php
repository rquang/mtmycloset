<?php

// Add the admin application columns
function mt_application_page_columns($columns)
{
    $columns = array(
        'cb' => '< input type="checkbox" />',
        'title' => 'ID',
        'app_closet' => 'Closet',
        'app_items' => 'Item(s)',
        'app_status' => 'Status',
        'date' => ('Date'),
    );
    return $columns;
}

// Populate the admin application columns
function mt_application_custom_columns($column)
{
    global $post;
    switch ($column) {
        case 'app_closet':
            $user_id = get_field("user_id", $post->ID);
            $user = get_userdata($user_id);
            echo $user->user_login;
            break;
        case 'app_items':
            $args = array(
                'post_type' => 'item',
                'orderby' => 'publish_date',
                'order' => 'ASC',
                'meta_query' => array(
                    array(
                        'key' => 'application_id', // name of custom field
                        'value' => $post->ID, // matches exactly "123", not just 123. This prevents a match for "1234"
                        'compare' => 'LIKE',
                    ),
                ),
            );
            $loop = new WP_Query($args);
            $count = 0;
            if ($loop->have_posts()):
                foreach ($loop->get_posts() as $p) {
                    ++$count;
                }
            endif;
            echo $count;
            break;
        case 'app_status':
            echo get_field("application_status", $post->ID);
            break;
    }
}
add_action("manage_application_posts_custom_column", "mt_application_custom_columns", 10, 2);
add_filter("manage_edit-application_columns", "mt_application_page_columns");
