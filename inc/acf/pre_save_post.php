<?php

/**
 * ACF Pre Save Post
 *
 * @return int $post_id The ID of the new post;
 */
function my_acf_pre_save_post($post_id)
{
    global $wp;

    $post_type = get_post_type($post_id);
    

    if ($post_id == ('user_' . get_current_user_id())) {
        do_action('acf/save_post', $post_id);
        $redirect = '/dashboard/size-chart';
        mt_redirect($redirect);
        return $post_id;
    }

    if ($post_id == 'new_influencer') {
        $user_id = get_current_user_id();
        $user = get_user_by( 'id', $user_id );
        // Create a new influencer application
        $post = array(
            'post_status' => 'publish',
            'post_title' => '',
            'post_type' => 'influencer',
        );
        // insert new influencer
        $post_id = wp_insert_post($post);
        wp_update_post(array(
            'ID' => $post_id,
            'post_title' => $user->user_login,
        ));
        // Update Item Status to Pending because ACF is dumb
        update_field('application_status', 'Pending', $post_id);
        update_field('user_id', $user_id, $post_id);
        cm_send_influencer_application_received($post_id);
        mt_email_new_influencer_application($user_id, $post_id);
        $redirect = '/dashboard/influencer/?u=1';
        mt_redirect($redirect);
        return $post_id;
    }

    if ($post_id == 'new_item') {
        if (!isset($_GET['app_id'])) {
            // Create a new application
            $post = array(
                'post_status' => 'draft',
                'post_title' => '',
                'post_type' => 'application',
            );
            // insert new application
            $application_id = wp_insert_post($post);

            // update title with id
            wp_update_post(array(
                'ID' => $application_id,
                'post_title' => $application_id,
            ));
        } else {
            $application_id = $_GET['app_id'];
        }

        // Create a new item
        $post = array(
            'post_status' => 'publish',
            'post_title' => '',
            'post_type' => 'item',
        );
        // insert new item
        $post_id = wp_insert_post($post);
        // update title with id
        wp_update_post(array(
            'ID' => $post_id,
            'post_title' => $post_id,
        ));

        // Update Item Status to Pending because ACF is dumb
        update_field('item_status', 'Pending', $post_id);

        // get closet id from current user
        $user_id = wp_get_current_user()->ID;
        // $args = array(
        //     'post_type' => 'closet',
        //     'meta_query' => array(
        //         array(
        //             'key' => 'user_id',
        //             'value' => $user_id,
        //             'compare' => 'LIKE',
        //         ),
        //     ),
        // );
        // $loop = new WP_Query($args);
        // if ($loop->have_posts()) {
        //     foreach ($loop->get_posts() as $p) {
        //         $user_id = $p->ID;
        //     }
        // }

        // save the fields to the post
        do_action('acf/save_post', $post_id);

        // update application id on item
        update_field('application_id', $application_id, $post_id);

        //update closet id on item
        update_field('user_id', $user_id, $post_id);

        //update closet id on application
        update_field('user_id', $user_id, $application_id);

    } else if ($post_id == $_GET['item_id']) {
        $application_id = $_GET['app_id'];
        // save the fields to the post
        do_action('acf/save_post', $post_id);
    } else {
        return $post_id;
    }

    // Set the application status to Pending because ACF is dumb
    update_field('application_status', 'Pending', $application_id);

    if (isset($_POST['submit_add'])) {
        wp_update_post(array(
            'ID' => $application_id,
            'post_status' => 'draft',
        ));
        $redirect = '/dashboard/sell/?app_id=' . $application_id;
        
        // $redirect = add_query_arg(array(
        //     'app_id' => $application_id,
        // ), home_url($wp->request));
    } else if (isset($_POST['submit_save'])) {
        wp_update_post(array(
            'ID' => $application_id,
            'post_status' => 'draft',
        ));
        // $redirect = '/dashboard/sell/?app_id=' . $application_id . '&item_id=' . $post_id;
        $redirect = '/dashboard/application/';
        // $redirect = add_query_arg(array(
        //     'app_id' => $application_id,
        //     'item_id' => $post_id,
        // ), home_url($wp->request));
    } else if (isset($_POST['submit_send'])) {
        wp_update_post(array(
            'ID' => $application_id,
            'post_status' => 'publish',
        ));

        // write_log($post_id);
        // $closet = get_field('user_id', $post_id);
        // write_log($closet);
        $user_id = wp_get_current_user()->ID;
        // write_log($user);

        // Send out new application notification email.
        mt_email_new_application($user_id, $application_id);
        cm_send_application_received($application_id);

        // mt_redirect('/dashboard/application');
        $redirect = '/dashboard/sell';
    }

    mt_redirect($redirect);
    exit;

    return $post_id;
}
add_filter('acf/pre_save_post', 'my_acf_pre_save_post', 10, 2);
