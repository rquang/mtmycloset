<?php

new acf_categories_select();

class acf_categories_select
{

    public function __construct()
    {
        // ajax action for loading city choices
        add_action('wp_ajax_load_categories_choices', array($this, 'ajax_load_categories_choices'));
        // enqueue js extension for acf
        // do this when ACF in enqueuing scripts
        add_action('acf/input/admin_enqueue_scripts', array($this, 'enqueue_script'));
    } // end public function __construct
    public function enqueue_script()
    {
        // enqueue acf extention

        // only enqueue the script on the post page where it needs to run
        /* *** THIS IS IMPORTANT
        ACF uses the same scripts as well as the same field identification
        markup (the data-key attribute) if the ACF field group editor
        because of this, if you load and run your custom javascript on
        the field group editor page it can have unintended side effects
        on this page. It is important to alway make sure you're only
        loading scripts where you need them.
         */
        global $post;
        // if (!$post ||
        //     !isset($post->ID) ||
        //     get_post_type($post->ID) != 'post') {
        //     return;
        // }

        $handle = 'my-acf-extension';
        $version = acf_get_setting('version');
        if (version_compare($version, '5.7.0', '<')) {
            // I'm using this method to set the src because
            // I don't know where this file will be located
            // you should alter this to use the correct functions
            // to set the src value to point to the javascript file
            $$src = '/' . str_replace(ABSPATH, '', dirname(__FILE__)) . '/my-acf-extension.js';
        } else {
            $src = get_stylesheet_directory_uri() . '/assets/js/dynamic-select-on-select.min.js';
        }
        // make this script dependent on acf-input
        $depends = array('acf-input');

        wp_enqueue_script($handle, $src, $depends);
    } // end public function enqueue_script

    public function ajax_load_categories_choices()
    {
        // this function is called by AJAX to load cities
        // based on state selection

        // we can use the acf nonce to verify
        if (!wp_verify_nonce($_POST['nonce'], 'acf_nonce')) {
            die();
        }
        $cat_id = 0;
        if (isset($_POST['cat_id'])) {
            $cat_id = intval($_POST['cat_id']);
        }

        $categories = $this->get_categories($cat_id);
        $choices = array();
        foreach ($categories as $value => $label) {
            $choices[] = array('value' => $value, 'label' => $label);
        }
        echo json_encode($choices);
        exit;
    } // end public function ajax_load_city_field_choices

    private function get_categories($parent_id)
    {
        // $post_id is post ID of state post
        // get all cities related to the state

        $args = array(
            'taxonomy' => 'product_cat',
            'hide_empty' => 0,
            'parent' => $parent_id,
        );

        // exclude formal, footwear categories
        if ($parent_id != 228 || $parent_id != 229) {
            $args['exclude'] = array(228, 229, 191, 258);
        }

        $categories = get_terms($args);
        $choices = [];
        if (!empty($categories)) {
            $choices = array('' => '-- Options --');
            // populate choices
            foreach ($categories as $category) {
                $choices[$category->term_id] = $category->name;
            }
        }
        return $choices;
    } // end private function get_cities

} // end class my_acf_extension
