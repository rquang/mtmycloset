<?php

add_action('acf/save_post', 'mt_acf_save_post', 20);

function mt_acf_save_post($post_id)
{
    global $wp;
    // bail early if not in admin
    if (!is_admin()) {
        return;
    } else {
        $post_type = get_post_type($post_id);
        switch ($post_type) {
            case 'application':
                mt_acf_application_save_post($post_id);
                break;
            case 'item':
                mt_acf_item_save_post($post_id);
                break;
            case 'influencer':
                mt_acf_influencer_save_post($post_id);
                break;
            default:
                return;
        }
    }
}

function mt_acf_item_save_post($post_id)
{
    $action = get_field('item_action', $post_id);
    switch ($action) {
        case 'Approve':
            update_field('item_status', 'Approved', $post_id);
            break;
        case 'Reject':
            update_field('item_status', 'Rejected', $post_id);
            break;
    }
    update_field('item_action', 'Pending', $post_id);
}

function mt_acf_application_save_post($post_id)
{
    // vars
    $post = get_post($post_id);

    $action = get_field('application_action', $post_id);

    switch ($action) {
        case 'Send Initial Invoice':
            cm_send_invoice($post_id);
            update_field('application_status', 'Initial Invoice - Sent', $post_id);
            break;
        case 'Send Shipping Label':
            cm_send_application_shipping_label($post_id);
            update_field('application_status', 'Shipping Label - Sent', $post_id);
            break;
        case 'Send Final Invoice':
            cm_send_final_invoice($post_id);
            update_field('application_status', 'Final Invoice - Sent', $post_id);
            break;
        case 'Reject':
            cm_send_application_rejected($post_id);
            update_field('application_status', 'Rejected', $post_id);
            break;
        case 'Publish Items':
            mt_publish_items($post_id);
            cm_send_application_product_up($post_id);
            update_field('application_status', 'Complete', $post_id);
            break;
    }

    update_field('application_action', 'Pending', $post_id);
}


function mt_acf_influencer_save_post($post_id)
{
    // vars
    $post = get_post($post_id);

    $action = get_field('application_status', $post_id);
    $user_id = get_field('user_id', $post_id);
    $user = get_user_by( 'id', $user_id );

    if ($post->post_title != $user->user_login) {
        wp_update_post(array(
            'ID' => $post_id,
            'post_title' => $user->user_login,
            'post_name' => ''
        ));
    }

    switch ($action) {
        case 'Approve':
            cm_send_influencer_application_approved($post_id);
            // DEV
            // mt_update_vendor_membership($user_id,766,5231);
            // LIVE
            mt_update_vendor_membership($user_id,766,7285);
            break;
        case 'Reject':
            cm_send_influencer_application_rejected($post_id);
            break;
    }

    update_field('application_action', 'Pending', $post_id);
}
