<?php
/**
 * Plugin Name: WCFM - Custom Menus
 * Plugin URI: http://wclovers.com
 * Description: WCFM Custom Menus.
 * Author: WC Lovers
 * Version: 1.0.0
 * Author URI: http://wclovers.com
 *
 * Text Domain: wcfm-custom-menus
 * Domain Path: /lang/
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.2.0
 *
 */

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

if (!class_exists('WCFM')) {
    return;
}
// Exit if WCFM not installed

/**
 * WCFM - Custom Menus Query Var
 */
function wcfmcsm_query_vars($query_vars)
{
    $wcfm_modified_endpoints = (array) get_option('wcfm_endpoints');

    $query_custom_menus_vars = array(
        'wcfm-applications' => !empty($wcfm_modified_endpoints['wcfm-applications']) ? $wcfm_modified_endpoints['wcfm-applications'] : 'applications',
        'wcfm-return' => !empty($wcfm_modified_endpoints['wcfm-return']) ? $wcfm_modified_endpoints['wcfm-return'] : 'return',
        // 'wcfm-donate' => !empty($wcfm_modified_endpoints['wcfm-donate']) ? $wcfm_modified_endpoints['wcfm-donate'] : 'donate',
        'wcfm-sell' => !empty($wcfm_modified_endpoints['wcfm-sell']) ? $wcfm_modified_endpoints['wcfm-sell'] : 'sell',
        'wcfm-influencer' => !empty($wcfm_modified_endpoints['wcfm-influencer']) ? $wcfm_modified_endpoints['wcfm-influencer'] : 'influencer',
        'wcfm-size-chart' => !empty($wcfm_modified_endpoints['wcfm-size-chart']) ? $wcfm_modified_endpoints['wcfm-size-chart'] : 'size-chart',
    );

    $query_vars = array_merge($query_vars, $query_custom_menus_vars);

    return $query_vars;
}
add_filter('wcfm_query_vars', 'wcfmcsm_query_vars', 50);

/**
 * WCFM - Custom Menus End Point Title
 */
function wcfmcsm_endpoint_title($title, $endpoint)
{
    global $wp;
    switch ($endpoint) {
        // case 'wcfm-donate':
        //     $title = __('Donate', 'wcfm-custom-menus');
        //     break;
        case 'wcfm-applications':
            $title = __('Applications', 'wcfm-custom-menus');
            break;
        case 'wcfm-return':
            $title = __('Returns', 'wcfm-custom-menus');
            break;
        case 'wcfm-sell':
            $title = __('Sell', 'wcfm-custom-menus');
            break;
        case 'wcfm-influencer':
            $title = __('Influencers', 'wcfm-custom-menus');
            break;
        case 'wcfm-size-chart':
            $title = __('Size Chart', 'wcfm-custom-menus');
            break;
    }

    return $title;
}
add_filter('wcfm_endpoint_title', 'wcfmcsm_endpoint_title', 50, 2);

/**
 * WCFM - Custom Menus Endpoint Intialize
 */
function wcfmcsm_init()
{
    global $WCFM_Query;

    // Intialize WCFM End points
    $WCFM_Query->init_query_vars();
    $WCFM_Query->add_endpoints();

    if (!get_option('wcfm_updated_end_point_cms')) {
        // Flush rules after endpoint update
        flush_rewrite_rules();
        update_option('wcfm_updated_end_point_cms', 1);
    }
}
add_action('init', 'wcfmcsm_init', 50);

/**
 * WCFM - Custom Menus Endpoint Edit
 */
function wcfm_custom_menus_endpoints_slug($endpoints)
{

    $custom_menus_endpoints = array(
        // 'wcfm-donate' => 'donate',
        'wcfm-applications' => 'applications',
        'wcfm-return' => 'return',
        'wcfm-sell' => 'sell',
        'wcfm-influencer' => 'influencer',
        'wcfm-size-chart' => 'size-chart',
    );

    $endpoints = array_merge($endpoints, $custom_menus_endpoints);

    return $endpoints;
}
add_filter('wcfm_endpoints_slug', 'wcfm_custom_menus_endpoints_slug');

if (!function_exists('get_wcfm_custom_menus_url')) {
    function get_wcfm_custom_menus_url($endpoint)
    {
        global $WCFM;
        $wcfm_page = get_wcfm_page();
        $wcfm_custom_menus_url = wcfm_get_endpoint_url($endpoint, '', $wcfm_page);
        return $wcfm_custom_menus_url;
    }
}

/**
 * WCFM - Custom Menus
 */
function wcfmcsm_wcfm_menus($menus)
{
    global $WCFM;
    $user = wp_get_current_user();

    $closet_menus = array(
        'wcfm-applications' => array('label' => __('Applications', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-applications'),
            'icon' => 'check',
            'priority' => 2,
        ),
        'wcfm-return' => array('label' => __('Returns', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-return'),
            'icon' => 'exchange-alt',
            'priority' => 4,
        ),
        'wcfm-influencer' => array('label' => __('Influencers', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-influencer'),
            'icon' => 'child',
            'priority' => 5,
        ),
    );

    $boutique_menus = array(
        'wcfm-size-chart' => array('label' => __('Size Chart', 'wcfm-custom-menus'),
            'url' => get_wcfm_custom_menus_url('wcfm-size-chart'),
            'icon' => 'columns',
            'priority' => 6,
        ),
    );

    if (wcfm_is_vendor() && apply_filters('wcfm_is_allow_vendor_membership', true) && !in_multi_array(138, mt_get_vendor_group()) ) {
        $menus = array_merge($menus, $closet_menus);
    } elseif  (wcfm_is_vendor() && apply_filters('wcfm_is_allow_vendor_membership', true) && in_multi_array(138, mt_get_vendor_group()) ) {
        $menus = array_merge($menus, $boutique_menus);
    }

    return $menus;
}
add_filter('wcfm_menus', 'wcfmcsm_wcfm_menus', 20);

/**
 *  WCFM - Custom Menus Views
 */
function wcfm_csm_load_views($end_point)
{
    global $WCFM, $WCFMu;
    $plugin_path = trailingslashit(get_stylesheet_directory() . '/inc/wcfm-custom-menus');

    switch ($end_point) {
        // case 'wcfm-donate':
        //     require_once $plugin_path . 'views/wcfm-views-donate.php';
        //     break;
        case 'wcfm-applications':
            require_once $plugin_path . 'views/wcfm-views-applications.php';
            break;
        case 'wcfm-return':
            require_once $plugin_path . 'views/wcfm-views-return.php';
            break;
        case 'wcfm-sell':
            require_once $plugin_path . 'views/wcfm-views-sell.php';
            break;
        case 'wcfm-influencer':
            require_once $plugin_path . 'views/wcfm-views-influencer.php';
            break;
        case 'wcfm-size-chart':
            require_once $plugin_path . 'views/wcfm-views-size-chart.php';
            break;
    }
}
add_action('wcfm_load_views', 'wcfm_csm_load_views', 50);
add_action('before_wcfm_load_views', 'wcfm_csm_load_views', 50);

// Custom Load WCFM Scripts
function wcfm_csm_load_scripts($end_point)
{
    global $WCFM;
    $plugin_url = trailingslashit(plugins_url('', __FILE__));

    switch ($end_point) {
        case 'wcfm-donate':
            wp_enqueue_script('wcfm_build_js', $plugin_url . 'js/wcfm-script-build.js', array('jquery'), $WCFM->version, true);
            break;
    }
}

// add_action('wcfm_load_scripts', 'wcfm_csm_load_scripts');
// add_action('after_wcfm_load_scripts', 'wcfm_csm_load_scripts');

// // Custom Load WCFM Styles
// function wcfm_csm_load_styles($end_point)
// {
//     global $WCFM, $WCFMu;
//     $plugin_url = trailingslashit(plugins_url('', __FILE__));

//     switch ($end_point) {
//         case 'wcfm-donate':
//             wp_enqueue_style('wcfmu_build_css', $plugin_url . 'css/wcfm-style-build.css', array(), $WCFM->version);
//             break;
//     }
// }
// add_action('wcfm_load_styles', 'wcfm_csm_load_styles');
// add_action('after_wcfm_load_styles', 'wcfm_csm_load_styles');

// /**
//  *  WCFM - Custom Menus Ajax Controllers
//  */
// function wcfm_csm_ajax_controller()
// {
//     global $WCFM, $WCFMu;

//     $plugin_path = trailingslashit(dirname(__FILE__));

//     $controller = '';
//     if (isset($_POST['controller'])) {
//         $controller = $_POST['controller'];

//         switch ($controller) {
//             case 'wcfm-donate':
//                 require_once $plugin_path . 'controllers/wcfm-controller-build.php';
//                 new WCFM_Build_Controller();
//                 break;
//         }
//     }
// }
// add_action('after_wcfm_ajax_controller', 'wcfm_csm_ajax_controller');
