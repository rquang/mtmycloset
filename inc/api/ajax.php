<?php
/**
 * Show cart contents / total Ajax
 */
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

    ?>
    <span class="mt-shopping-bag-count">
        <?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count); ?>
    </span>
	<?php
$fragments['span.mt-shopping-bag-count'] = ob_get_clean();
    return $fragments;
}

function mt_application_item_ajax_request()
{
    if (isset($_REQUEST)) {
        // acf_enqueue_uploader();
        $options = array(
            'post_id' => $_POST['item_id'],
            'post_title' => false,
            'form' => false,
            'field_groups' => array(
                $_POST['field_group_id'],
            ),
            'fields' => false,
            // 'updated_message' => __("Item Updated", 'acf'),
            // 'submit_value' => __("Update Item", 'acf'),
            'uploader' => 'wp',
        );
        acf_form($options);
    }
    die();
}

add_action('wp_ajax_mt_application_item_ajax_request', 'mt_application_item_ajax_request');
?>