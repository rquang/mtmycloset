<?php

add_filter('wcfm_is_allow_store_name', '__return_false');

add_filter('wcfm_is_allow_login_redirect', '__return_false');

add_filter( 'wcfm_is_allow_email_verification', '__return_false' );

add_filter( 'wcfm_is_allow_store_setup', '__return_false' );

//create a new closet and attach to user on new registrations.
add_action('user_register', 'mt_new_registration', 10, 1);

function mt_new_registration($user_id) {
    $membership = get_user_meta( $user_id, 'wcfm_membership' , true );
    $group = get_user_meta( $user_id, '_wcfm_vendor_group' , true );
    // write_log('new_register ' . $user_id);
    // write_log('new_membership ' . $membership);
    // write_log('new_group ' . $group);
    if( $membership == '' || $group == '' ) {
        mt_update_vendor_membership($user_id,24,6963);
    }
}

// function mt_new_registration($user_id)
// {

//     $user = get_userdata($user_id);

//     $post = array(
//         'post_type' => 'closet', // Your post type ( post, page, custom post type )
//         'post_status' => 'publish', // (publish, draft, private, etc.)
//         'post_title' => wp_strip_all_tags($user->data->display_name),
//     );
//     // insert the post
//     $post_id = wp_insert_post($post);

//     update_field('user_id', $user_id, $post_id);
// }

// apply_filters('wcfm_products_actions', $actions, $the_product);


add_filter('wcfm_products_actions', 'mt_product_actions', 10, 2);


function mt_product_actions($actions, $product)
{
    if (!$product) {
        return $actions;
    }

    if ( in_array( 'administrator', wp_get_current_user()->roles ) || in_array( 'shop_manager', wp_get_current_user()->roles ) || in_multi_array(138, mt_get_vendor_group()) ) {
        return $actions;
    }

    $regular_price = number_format((float) $product->get_regular_price(), 2, '.', '');
    $sale_price = number_format((float) $product->get_sale_price(), 2, '.', '');

    $ID = $product->get_id();

    if ($sale_price > 150.00 || ($regular_price > 150.00 && $sale_price == 0)) {
        $actions .= '<a class="wcfm_product_lower_price wcfm-mt-action-icon" href="' . get_stylesheet_directory_uri() . '/inc/item.php?action=1&id=' . $ID . '" data-proid="' . $ID . '"><span class="text_tip" data-tip="Lower Price By 10%">Lower Price</span></a>';
    }

    if ($product->get_date_created()) {
        $start_date = $product->get_date_created()->format('Y-m-d');
        $end_date = current_time('Y-m-d');
        $date_diff = floor((strtotime($start_date) - strtotime($end_date)) / 3600 / 24 * -1);
        if ($date_diff >= 10) {
            $actions .= '<a class="wcfm_product_return wcfm-mt-action-icon" href="#" data-proid="' . $ID . '"><span class="text_tip" data-tip="Return Item Back To You. Shipping Rates will Apply.">Return Item</span></a>';
            $actions .= '<a class="wcfm_product_return wcfm-mt-action-icon" href="#" data-proid="' . $ID . '"><span class="text_tip" data-tip="Donate Item to Goodwill">Donate Item</span></a>';
        
        
        }
    }

    return $actions;
}

add_filter('wcfm_products_date_display', 'mt_product_date', 10, 1);

function mt_product_date($start_date) {
    $start_date = date("Y-m-d", strtotime($start_date));
    $end_date = current_time('Y-m-d');
    $date_diff = floor((strtotime($start_date) - strtotime($end_date)) / 3600 / 24 * -1);

    return $start_date . '<br>' . date_i18n($date_diff) . ' days ago';
}

function mt_get_product_vendor_group($id = null) {
    global $product;
    if (null === $id) {
        $id = get_user_by( 'id', $product->post->post_author )->ID;
    }
    return get_user_meta($id,'_wcfm_vendor_group');
}

function mt_get_vendor_group($id = null) {
    if (null === $id) {
        $id = get_current_user_id();
    }
    return get_user_meta($id,'_wcfm_vendor_group');
}

function mt_vendor_location_check() {
    $user_id = get_current_user_id();
    if (!get_user_meta($user_id,'_wcfm_street_1')[0]) {
        return false;
    }
    if (!get_user_meta($user_id,'_wcfm_city')[0]) {
        return false;
    }
    if (!get_user_meta($user_id,'_wcfm_zip')[0]) {
        return false;
    }
    if (!get_user_meta($user_id,'_wcfm_country')[0]) {
        return false;
    }
    if (!get_user_meta($user_id,'_wcfm_state')[0]) {
        return false;
    }
    return true;
}

function mt_vendor_name_check() {
    $user_id = get_current_user_id();
    $user = get_userdata($user_id);
    if (!$user->user_firstname) {
        return false;
    }
    if (!$user->user_lastname) {
        return false;
    }
    return true;
}

function mt_get_vendor_country() {
    $user_id = get_current_user_id();
    return get_user_meta($user_id,'_wcfm_country')[0];
}

add_filter( 'wcfmmp_stores_args', 'mt_wcfmmp_stores_args', 1, 1);

function mt_wcfmmp_stores_args($template_args) {
	global $WCFM, $WCFMmp, $wp, $WCFM_Query, $includes;

	// if (!is_page(5110) && !is_page(5190) ) {
    if (!is_page(5110)) {
            return $template_args;
	}

	$include_members = array();
	$include_membership = array(137,139,140,141,10029);
	// $include_membership = isset( $attr['include_membership'] ) ? sanitize_text_field( $attr['include_membership'] ) : '';
	if( !empty( $include_membership ) && is_array( $include_membership ) ) {
		foreach( $include_membership as $wcfm_membership ) {
			$membership_users = (array) get_post_meta( $wcfm_membership, 'membership_users', true );
			$membership_users = array_filter($membership_users);
			$include_members  = array_merge( $include_members, $membership_users );
		}
	}
	// $includes = array_merge($template_args['includes'],$include_members);

	$stores = array();
	foreach( $include_members as $member ) {
		$user = get_userdata($member);
		$stores[$member] = $user->user_login;
	}

    $template_args['stores'] = $stores;
	return $template_args;
}

add_filter( 'wcfm_membership_payment_methods', 'mt_wcfm_membership_payment_methods', 1, 1);

function mt_wcfm_membership_payment_methods($array) {
    $array['stripe'] = __( 'Credit Card', 'wc-multivendor-membership' );
    return $array;
}

// update the vendor with the specific membership id
function mt_update_vendor_membership($user_id = null, $membership_id, $group_id) {
    if (null === $user_id) {
        $user_id = get_current_user_id();
    }
    // $membership = get_user_meta($user_id, 'wcfm_membership');
    update_user_meta( $user_id, 'wcfm_membership', $membership_id );
    $vendor_groups[] = $group_id;
    $vendor_groups = array_unique($vendor_groups);
    update_user_meta( $user_id, '_wcfm_vendor_group', $vendor_groups);
    $group_vendors = (array) get_post_meta( $group_id, '_group_vendors', true );
    $group_vendors[] = $user_id;
    $group_vendors = array_unique($group_vendors);
    update_post_meta($group_id, '_group_vendors', $group_vendors);
}

add_filter( 'wcfm_social_url', 'mt_wcfm_generate_social_url', 1, 2);

function mt_wcfm_generate_social_url( $social_handle, $social ) {
	// switch( $social ) {
	// 	case 'facebook':
	// 	  if( strpos( $social_handle, 'facebook' ) === false) {
	// 	  	$social_handle = 'https://facebook.com/' . $social_handle;
	// 	  }
	// 	break;
		
	// 	case 'twitter' :
	// 	  if( strpos( $social_handle, 'twitter' ) === false) {
	// 	  	$social_handle = 'https://twitter.com/' . $social_handle;
	// 	  }
	// 	break;
		
	// 	case 'instagram' :
	// 	  if( strpos( $social_handle, 'instagram' ) === false) {
	// 	  	$social_handle = 'https://instagram.com/' . $social_handle;
	// 	  }
	// 	break;
		
	// 	case 'linkedin' :
	// 	  if( strpos( $social_handle, 'linkedin' ) === false) {
	// 	  	//$social_handle = 'https://linkedin.com/' . $social_handle;
	// 	  }
	// 	break;
		
	// 	case 'youtube' :
	// 	  if( strpos( $social_handle, 'youtube' ) === false) {
	// 	  	$social_handle = 'https://youtube.com/channel/' . $social_handle;
	// 	  }
	// 	break;
    // }

    if( strpos( $social_handle, 'https://' ) === false && strpos( $social_handle, 'http://' ) === false) {
        $social_handle = 'https://' . $social_handle;
    }
    return $social_handle;
}

add_filter( 'wcfm_membership_registration_fields', 'mt_wcfm_membership_registration_fields', 1, 1);

function mt_wcfm_membership_registration_fields($args) {
    $args['first_name']['custom_attributes'] =  array( 'required' => 1 );
    $args['last_name']['custom_attributes'] =  array( 'required' => 1 );
    return $args;
}
