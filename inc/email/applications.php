<?php

function mt_internal_email($post_id, $subject, $body)
{
    // email data
    $to = array(
        'info@mtmycloset.com'
        // 'rquang@socialhero.ca',
    );
    $from = 'info@mtmycloset.com';
    $headers = 'From: MT MyCloset <' . $from . '>' . "\r\n";

    // send email
    wp_mail($to, $subject, $body, $headers);
}

function mt_email_new_application($user_id, $post_id)
{
    $user = get_userdata($user_id);
    $subject = 'Application #' . $post_id . ' - Ready for Review';
    $body = 'A new application that has been submitted by ' . $user->user_login . ' is ready for review - ' . get_edit_post_link($post);
    mt_internal_email($post_id, $subject, $body);
}

function mt_email_reviewed_application_initial($user_id, $post_id)
{
    $user = get_userdata($user_id);
    $subject = 'Application #' . $post_id . ' - Initial Invoice Reviewed By Seller';
    $body = 'Application #' . $post_id . ' - has been reviewed by ' . $user->user_login . ' is ready for the next step - ' . get_edit_post_link($post);
    mt_internal_email($post_id, $subject, $body);
}

function mt_email_reviewed_application_final($user_id, $post_id)
{
    $user = get_userdata($user_id);
    $subject = 'Application #' . $post_id . ' - Final Invoice Reviewed By Seller';
    $body = 'Application #' . $post_id . ' - has been reviewed by ' . $user->user_login . ' is ready for the next step - ' . get_edit_post_link($post);
    mt_internal_email($post_id, $subject, $body);
}


function mt_email_new_influencer_application($user_id, $post_id)
{
    $user = get_userdata($user_id);
    $subject = 'Influencer Application #' . $post_id . ' - Ready for Review';
    $body = 'A new influencer application that has been submitted by ' . $user->user_login . ' is ready for review - ' . get_edit_post_link($post);
    mt_internal_email($post_id, $subject, $body);
}
