<?php

function grosso_ajax_search() {

    unset($_REQUEST['action']);
    if (empty($_REQUEST['s'])) {
        $_REQUEST['s'] = array_shift(array_values($_REQUEST));
    }
    if (empty($_REQUEST['s'])) {
        wp_die();
    }

    $defaults = array(
            'numberposts' => 5,
            'post_type' => 'any',
            'post_status' => 'publish',
            'post_password' => '',
            'suppress_filters' => false
    );

    $_REQUEST['s'] = apply_filters('get_search_query', $_REQUEST['s']);

    $users = new WP_User_Query( array(
        'search'         => '*' . esc_attr( $_REQUEST['s'] ) . '*',
        'search_columns' => array(
            'user_login',
            'user_nicename',
        ),
    ) );
    $users_found = $users->get_results();
    $user_count = 0;

    if ($users_found) {
        $users_array = [];
        foreach ( $users_found as $user ) {
            if ($user->roles[0] != 'wcfm_vendor') {
                continue;
            }
            $user_post = new stdClass();
            $user_post->ID = $user->ID;
            $user_post->post_author = -99;
            $user_post->post_date = current_time( 'mysql' );
            $user_post->post_date_gmt = current_time( 'mysql', 1 );
            $user_post->post_title = $user->user_login;
            $user_post->post_content = '';
            $user_post->post_status = 'publish';
            $user_post->comment_status = 'closed';
            $user_post->ping_status = 'closed';
            $user_post->post_name = 'user-post-' . rand( 1, 99999 ); // append random number to avoid clash
            $user_vendor_group = mt_get_vendor_group($user->ID);
            if ( searchMultiArray(6963, $user_vendor_group) ) {
                $user_post->post_type = 'Closets';
            } elseif ( searchMultiArray(7285, $user_vendor_group) ) {
                $user_post->post_type = 'Influencers';
            } elseif ( searchMultiArray(138, $user_vendor_group) ) {
                $user_post->post_type = 'Boutiques';
            }
            $user_post->filter = 'raw'; // important!
            $users_array[] = $user_post;
            $user_count++;
        }
    }

    $tags_found = new WP_Term_Query( array(
        'taxonomy' => 'product_tag',
        'hide_empty' => true,
        'name__like' => esc_attr( $_REQUEST['s'] ),
    ) );
    $tag_count = 0;

    if ( ! empty( $tags_found->terms ) ) {
        $tag_array = [];
        foreach ( $tags_found->terms as $term ) {
            $tag_post = new stdClass();
            $tag_post->ID = $term->term_id;
            $tag_post->post_author = -99;
            $tag_post->post_date = current_time( 'mysql' );
            $tag_post->post_date_gmt = current_time( 'mysql', 1 );
            $tag_post->post_title = $term->name;
            $tag_post->post_content = '';
            $tag_post->post_status = 'publish';
            $tag_post->comment_status = 'closed';
            $tag_post->ping_status = 'closed';
            $tag_post->post_name = 'term-post-' . rand( 1, 99999 ); // append random number to avoid clash
            $tag_post->post_type = 'Tags';
            $tag_post->filter = 'raw'; // important!
            $tag_array[] = $tag_post;
            $tag_count++;
        }
    }

    $parameters = array_merge($defaults, $_REQUEST);
    $query = http_build_query($parameters);
    $result = get_posts($query);

    if ($users_found && $user_count != 0) {
        $result = array_merge($result, $users_array);
    }

    if ($tag_count != 0) {
        $result = array_merge($result, $tag_array);
    }

    // If there are WC products in the result and visibility is not set for search - remove them
    if(GROSSO_IS_WOOCOMMERCE) {
        foreach ( $result as $key => $post ) {
            $product = wc_get_product( $post );
            if ( is_a($product, 'WC_Product') && !('visible' === $product->get_catalog_visibility() || 'search' === $product->get_catalog_visibility()) ) {
                unset($result[$key]);
            }
        }
    }

    $search_messages = array(
            'no_criteria_matched' => esc_html__("Sorry, no posts matched your criteria", 'grosso'),
            'another_search_term' => esc_html__("Please try another search term", 'grosso'),
            'time_format' => esc_attr(get_option('date_format')),
            'all_results_query' => http_build_query($_REQUEST),
            'all_results_link' => esc_url(home_url('?' . http_build_query($_REQUEST))),
            'view_all_results' => esc_html__('View all results', 'grosso')
    );

    if (empty($result)) {
        $output = "<ul>";
        $output .= "<li>";
        $output .= "<span class='ajax_search_unit ajax_not_found'>";
        $output .= "<span class='ajax_search_content'>";
        $output .= "    <span class='ajax_search_title'>";
        $output .= $search_messages['no_criteria_matched'];
        $output .= "    </span>";
        $output .= "    <span class='ajax_search_excerpt'>";
        $output .= $search_messages['another_search_term'];
        $output .= "    </span>";
        $output .= "</span>";
        $output .= "</span>";
        $output .= "</li>";
        $output .= "</ul>";
        echo wp_kses_post($output);
        wp_die();
    }

    // reorder posts by post type
    $output = "";
    $sorted = array();
    $post_type_obj = array();
    $closet_arr = array("Closets", "Influencers", "Boutiques");
    foreach ($result as $post) {
        $sorted[$post->post_type][] = $post;
        if (empty($post_type_obj[$post->post_type])) {
            if (in_array($post->post_type, $closet_arr) || $post->post_type == 'Tags') {
                $custom_post_type = new stdClass();
                $custom_post_type_labels = new stdClass();
                $custom_post_type_labels->name = $post->post_type;
                $custom_post_type->labels = $custom_post_type_labels;
                $post_type_obj[$post->post_type] = $custom_post_type;
            } else {
                $post_type_obj[$post->post_type] = get_post_type_object($post->post_type);
            }
        }
    }

    //preapre the output
    foreach ($sorted as $key => $post_type) {
        if (isset($post_type_obj[$key]->labels->name)) {
            $label = $post_type_obj[$key]->labels->name;
            $output .= "<h4>" . esc_html($label) . "</h4>";
        } else {
            $output .= "<hr />";
        }

        $output .= "<ul>";

        foreach ($post_type as $post) {
            if ( in_array($post->post_type, $closet_arr) ) {
                $str = urlencode($post->post_title);
                $str = str_replace('.', '-', $str);
                $str = str_replace('+', '-', $str);
                $link = '/closet/' . $str;
                $title = $post->post_title;
            } else if ( $post->post_type == 'Tags' ) {
                $link = '/tag/' . urlencode($post->post_title);
                $title = $post->post_title;
            } else {
                $image = get_the_post_thumbnail($post->ID, 'grosso-widgets-thumb');
                $link = get_permalink($post->ID);
                $title = get_the_title($post->ID);
            }

            $excerpt = "";

            if ( !in_array($post->post_type, $closet_arr) && $post->post_type != 'Tags' ) {
                if (!empty($post->post_excerpt)) {
                    $excerpt = grosso_generate_excerpt($post->post_excerpt, 70, " ", "...", true, '', true);
                } else {
                    $excerpt = get_the_time($search_messages['time_format'], $post->ID);
                }
            }

            $output .= "<li>";
            $output .= "<a class ='ajax_search_unit' href='" . esc_url($link) . "'>";
            if ($image) {
                $output .= "<span class='ajax_search_image'>";
                $output .= $image;
                $output .= "</span>";
            }
            $output .= "<span class='ajax_search_content'>";
            $output .= "    <span class='ajax_search_title'>";
            $output .= $title;
            $output .= "    </span>";
            $output .= "    <span class='ajax_search_excerpt'>";
            $output .= $excerpt;
            $output .= "    </span>";
            $output .= "</span>";
            $output .= "</a>";
            $output .= "</li>";
        }

        $output .= "</ul>";
    }

    $output .= "<a class='ajax_search_unit ajax_search_unit_view_all' href='" . esc_url($search_messages['all_results_link']) . "'>" . esc_html($search_messages['view_all_results']) . "</a>";

    echo wp_kses_post($output);
    wp_die();
}