<?php
// add_filter('acf/pre_save_post', 'save_post_from_frontend');
// function save_post_from_frontend($post_id) {
//     //Check if user loggin or can publish post
//     // if( ! ( is_user_logged_in() || current_user_can('publish_posts') ) ) {
//     //         return;
//     // }

//     // check if this is to be a new post
//     if( $post_id!= 'new_post') {
//         return $post_id;
//     }

//     $post= array(
//         'post_type'     =>    'application', // Your post type ( post, page, custom post type )
//         'post_status'   =>    'publish', // (publish, draft, private, etc.)
//         'post_title'    =>    'Test'
//     );
//     // insert the post
//     $post_id= wp_insert_post( $post);
//     // Save the fields to the post
//     do_action( 'acf/save_post', $post_id);
//     return $post_id;
// }

add_action('wp_head', 'mt_add_acf_form_functionality', 2);

function mt_add_acf_form_functionality()
{
    acf_form_head();

    // if (is_page('sell-application') || is_wc_endpoint_url('closet')) {
    //   acf_form_head();
    // }
}

require_once get_stylesheet_directory() . '/inc/acf/pre_save_post.php';
require_once get_stylesheet_directory() . '/inc/acf/save_post.php';
require_once get_stylesheet_directory() . '/inc/acf/categories_select.php';

// disable acf css on front-end acf forms
// add_action('wp_print_styles', 'my_deregister_styles', 100);

function my_deregister_styles()
{
    wp_deregister_style('acf');
    wp_deregister_style('acf-field-group');
    wp_deregister_style('acf-global');
    wp_deregister_style('acf-input');
    wp_deregister_style('acf-datepicker');
}

function mt_bootstrap_acf_field($field)
{
    if (!is_admin()) {
        $field['class'] = 'form-control';
        $field['wrapper']['class'] .= ' form-group';
    }

    return $field;
}

// ALL
add_filter('acf/load_field', 'mt_bootstrap_acf_field');

// add_action('init', 'blockusers_init');
// function blockusers_init()
// {
//     if (is_admin() && !current_user_can('administrator') &&
//         !(defined('DOING_AJAX') && DOING_AJAX)) {
//         wp_redirect(home_url());
//         exit;
//     }
// }

//Allow Contributors to Add Media
// if (current_user_can('wcfm_vendor') && !current_user_can('upload_files')) {
//     add_action('admin_init', 'allow_contributor_uploads');
// }

// function allow_contributor_uploads()
// {
//     $contributor = get_role('wcfm_vendor');
//     $contributor->add_cap('upload_files');
// }
