<?php

// Remove Publish field
// add_action('admin_menu', function () {
//     remove_meta_box('submitdiv', 'application', 'side');
//     remove_meta_box('submitdiv', 'closet', 'side');
//     remove_meta_box('submitdiv', 'item', 'side');
// });

function disable_new_posts()
{
    // Hide sidebar link
    global $submenu;
    unset($submenu['edit.php?post_type=application'][10]);
    unset($submenu['edit.php?post_type=item'][10]);

    $cpt = array("application", "item");

    // Hide link on listing page
    if (isset($_GET['post_type']) && in_array($_GET['post_type'], $cpt)) {
        echo '<style type="text/css">
        #favorite-actions, .add-new-h2, .page-title-action { 
            display:none !important; 
        }
        </style>';
    }
}
add_action('admin_menu', 'disable_new_posts');

add_action('admin_head', 'custom_admin_css');
function custom_admin_css() {
    echo '<style type="text/css">
    .is--disabled {
        pointer-events: none !important;
    }
    .is--disabled input, .is--disabled select { 
        -moz-appearance: window !important;
        -webkit-appearance: none !important;
    }
    .is--disabled .select2-selection__arrow, .is--hidden {
        display: none !important;
    }
    .post-type-application .page-title-action, .post-type-item .page-title-action {
        display: none !important;
    }
    </style>';
}

// function disable_acf_load_field( $field ) {
//     $fields = "project_code","project_type","project_status","funding_year"; // add more here
//     if(in_array($field["name"],$fields)) $field['disabled'] = true;
//     return $field;
// }
// add_filter('acf/load_field/type=text', 'disable_acf_load_field');

// Register styles and scripts to admin end
function wpse_cpt_enqueue($hook_suffix)
{
    $cpt = array("application", "item");

    if (in_array($hook_suffix, array('post.php', 'post-new.php'))) {
        $screen = get_current_screen();

        if (is_object($screen) && in_array($screen->post_type, $cpt)) {
            // acf_form_head();
            wp_enqueue_script('application-admin-script', get_stylesheet_directory_uri() . '/assets/js/application.min.js', array('jquery'), null, true);
        }
    }
}

add_action('admin_enqueue_scripts', 'wpse_cpt_enqueue');

// Grabs the inserted post data so you can modify it.
function modify_post_title($data, $postarr)
{
    $cpt = array("application", "item");
    if (in_array($data['post_type'], $cpt)) {
        $data['post_title'] = $postarr['ID']; //Updates the post title to your new title.
    }
    return $data; // Returns the modified data.
}
add_filter('wp_insert_post_data', 'modify_post_title', '99', 2);

require_once get_stylesheet_directory() . '/inc/admin/columns.php';
require_once get_stylesheet_directory() . '/inc/admin/metabox.php';
