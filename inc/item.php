<?php

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

global $product, $woocommerce, $woocommerce_loop;

// foreach ($_POST as $key => $value):
//     $product_id = $key;
//     $action = $value;
// endforeach;

$product_id = $_GET['id'];
$action = $_GET['action'];

switch ($action) {
    case 1:
        mt_lower_product_price($product_id);
        mt_redirect('/dashboard/products');
        break;
    // case 2:
    //     mt_promote_product($product_id);
    //     break;
    case 3:
        mt_return_product($product_id);
        mt_redirect('/dashboard/products');
        break;
        // case 4:
        //     mt_donate_product($product_id);
        //     break;
}

function mt_donate_product($ID)
{
    $product = wc_get_product($ID);
    // Create a new item
    $post = array(
        'post_status' => 'publish',
        'post_title' => '',
        'post_type' => 'donate',
    );
    // insert new item
    $post_id = wp_insert_post($post);
    // update title with id
    wp_update_post(array(
        'ID' => $post_id,
        'post_title' => $post_id,
    ));
    // update product_id field with product ID
    update_field('product_id', $ID, $post_id);
    // update product status
    update_field('product_status', 'Donate', $ID);
}

function mt_return_product($ID)
{
    $product = wc_get_product($ID);
    // Create a new item
    $post = array(
        'post_status' => 'publish',
        'post_title' => '',
        'post_type' => 'return',
    );
    // insert new item
    $post_id = wp_insert_post($post);
    // update title with id
    wp_update_post(array(
        'ID' => $post_id,
        'post_title' => $post_id,
    ));
    // update product_id field with product ID
    update_field('product_id', $ID, $post_id);
    // update product status
    update_field('product_status', 'Return', $ID);
}

function mt_lower_product_price($ID)
{
    $product = wc_get_product($ID);
    if ($product->is_on_sale()):
        $price = $product->get_sale_price();
    else:
        $price = $product->get_regular_price();
    endif;

    write_log($price);

    $new_price = round($price * 0.9, 2);

    write_log($new_price);

    if ($new_price < 150) {
        $new_price = 150;
    }

    update_post_meta($ID, '_sale_price', $new_price);
    update_post_meta($ID, '_price', $new_price);

}
