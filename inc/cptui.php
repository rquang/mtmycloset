<?php
function wpse_277843_cptui_ep_mask( $args, $taxonomy_slug, $taxonomy_args ) {

    $taxonomies = array("mens", "mens_categories", "womens", "womens_categories", "formal_mens_categories", "formal_womens_categories");

    if ( in_array($taxonomy_slug, $taxonomies) ) {
        $args['rewrite']['ep_mask'] = EP_TAGS;
    }

    return $args;
}
add_filter( 'cptui_pre_register_taxonomy', 'wpse_277843_cptui_ep_mask', 10, 3 );

function wpse_277843_all_endpoint() {
    add_rewrite_endpoint( 'all', EP_TAGS );
}
add_action( 'init', 'wpse_277843_all_endpoint' );

function wpse_277843_all_posts( $query ) {

    $taxonomies = array("mens", "mens_categories", "womens", "womens_categories", "formal_mens_categories", "formal_womens_categories");

    // write_log($query);
    // write_log($query->is_main_query());
    // write_log($query->is_tax( $taxonomies ));
    // write_log($query->is_category( $taxonomies ));

    if ( $query->is_main_query() && $query->is_tax( $taxonomies ) ) {
        write_log('shit');
        if ( isset( $query->query_vars['all'] ) ) {
            $query->set( 'posts_per_page', -1 );
        }
    }
}
add_action( 'pre_get_posts', 'wpse_277843_all_posts' );