<?php

function cm_send_transactional($smart_email_id, $message) {
    require_once get_stylesheet_directory() . '/inc/campaignmonitor/csrest_transactional_smartemail.php';
    $auth = array("api_key" => $GLOBALS['cm_api_key']);
    $wrap = new CS_REST_Transactional_SmartEmail($smart_email_id, $auth);
    $consent_to_track = 'no'; # Valid: 'yes', 'no', 'unchanged'
    $result = $wrap->send($message, $consent_to_track);
}

// Send transactional email
function cm_send_invoice($post_id)
{
    $smart_email_id = 'db2080e7-534f-48be-b039-1cf978b002d4';

    $application_notes = get_field('application_notes', $post_id);
    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);
    $application_link = 'https://mtmycloset.com/dashboard/applications?app_id=' . $post_id;

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            "username" => $user->display_name,
            "firstname" => $user->user_firstname,
            "lastname" => $user->user_lastname,
            "app_id" => $post_id,
            "app_link" => $application_link,
            // "app_notes" => $application_notes,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_final_invoice($post_id)
{
    $smart_email_id = 'd80d6dae-a628-42a4-a4d2-b8803a236380';

    $application_notes = get_field('application_notes', $post_id);
    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);
    $application_link = 'https://mtmycloset.com/dashboard/applications?app_id=' . $post_id;

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'app_id' => $post_id,
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            "firstname" => $user->user_firstname,
            'app_link' => $application_link,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_application_received($post_id)
{
    $smart_email_id = 'bd058a4c-7714-47ac-b524-6eea6882349b';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'app_id' => $post_id,
            'firstname' => $user->user_firstname,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_influencer_application_received($post_id)
{
    $smart_email_id = '8adef005-a742-46ec-90af-70214838db82';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),        
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_influencer_application_approved($post_id)
{
    $smart_email_id = 'ae7a42bf-cc5b-4e6f-afa2-d1a3a4aa73fe';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
    );
    
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_influencer_application_rejected($post_id)
{
    $smart_email_id = '3118bfe9-0d81-4961-b19a-9576b2ad69b6';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_application_shipping_label($post_id)
{
    $smart_email_id = '25cdc688-1fab-45c5-9cfb-34b2924971a6';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $application_link = 'https://mtmycloset.com/dashboard/applications';

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'app_id' => $post_id,
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
            'app_link' => $application_link,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_application_product_up($post_id)
{
    $smart_email_id = '64d91c28-ca53-47f2-8213-9074cd5b4d67';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'app_id' => $post_id,
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
            'closet_link' => $closet_link,
        ),
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_30_day($user_id)
{
    $smart_email_id = 'c271bab7-a103-4876-ba90-b9c3bca3daad';

    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
        
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_60_day($user_id)
{
    $smart_email_id = '4e022a43-99ff-4608-a502-714552ebba49';

    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
        
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_90_day($user_id)
{
    $smart_email_id = 'bda3d618-cb4e-4a60-ad3c-6314c13f4f7d';

    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
        
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_14_day($user_id)
{
    $smart_email_id = '5d7f64e2-45cc-4757-815a-e8ba4f32d496';

    $user = get_userdata($user_id);

    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'firstname' => $user->user_firstname,
        ),
        
    );
    cm_send_transactional($smart_email_id, $message);
}

function cm_send_application_rejected($post_id)
{
    $smart_email_id = 'ab68510c-0a3c-4d82-aefd-1a3e6b9d9633';

    $user_id = get_field('user_id', $post_id);
    $user = get_userdata($user_id);
    
    $to = $user->user_firstname . ' ' . $user->user_lastname . '<' . $user->user_email . '>';
    $closet_link = 'https://mtmycloset.com/closet/' . $user->user_login;

    $message = array(
        "To" => array(
            $to,
        ),
        "Data" => array(
            'x-apple-data-detectors' => 'x-apple-data-detectorsTestValue',
            'href^="tel"' => 'href^="tel"TestValue',
            'href^="sms"' => 'href^="sms"TestValue',
            'owa' => 'owaTestValue',
            'role=section' => 'role=sectionTestValue',
            'style*="font-size:1px"' => 'style*="font-size:1px"TestValue',
            'app_id' => $post_id,
            'firstname' => $user->user_firstname,
        ),
        
    );
    cm_send_transactional($smart_email_id, $message);
}