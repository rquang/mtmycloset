<?php
global $WCFM, $wp_query;

?>

<div class="collapse wcfm-collapse" id="wcfm_sell_listing">

	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-check"></span>
		<span class="wcfm-page-heading-text"><?php _e('Applications', 'wcfm-custom-menus');?></span>
		<?php do_action('wcfm_page_heading');?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action('before_wcfm_service');?>

		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('Applications', 'wcfm-custom-menus');?></h2>
			<?php if (mt_vendor_location_check() && mt_vendor_name_check() ) : ?>
				<a id="add_new_application_dashboard" class="add_new_wcfm_ele_dashboard text_tip"
					href="/dashboard/sell/" data-tip="Add New Application" data-hasqtip="39"
					aria-describedby="qtip-39"><span class="wcfmfa fa-list"></span><span class="text">New Application</span></a>
			<?php endif; ?>
			<div class="wcfm-clearfix"></div>
		</div>
		<div class="wcfm-clearfix"></div><br />

		<div class="wcfm-container">
			<?php
			if (mt_vendor_location_check() && mt_vendor_name_check() ) {
				if (!empty($_POST) && empty($_POST['tracking_number'])):
					get_template_part('/templates/account/applications/submit');
				elseif (!isset($_GET['app_id'])):
					get_template_part('/templates/account/applications/applications');
				else:
					get_template_part('/templates/account/applications/single');
				endif;
			} else {
				if (!mt_vendor_location_check() ) {
				?>
					<div class="container py-5">
						<p>
							Please update your location information before submitting a sell application.
						</p>
						<a href="/dashboard/settings" class="primary-btn">Update Location Settings</a>
					</div>
				<?php
				}
				if (!mt_vendor_name_check() ) {
					?>
						<div class="container py-5">
							<p>
								Please update your personal information before submitting a sell application.
							</p>
							<a href="/dashboard/profile" class="primary-btn">Update Personal Settings</a>
						</div>
					<?php
				}
			}
			?>

			<div class="wcfm-clearfix"></div>
			<?php
			do_action('after_wcfm_service');
			?>
		</div>
	</div>
</div>

<style>
#wcfm_applications_listing {
	display: none !important;
}
</style>
