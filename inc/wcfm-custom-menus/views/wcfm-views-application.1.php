<?php
global $WCFM, $wp_query;

?>

<div class="collapse wcfm-collapse" id="wcfm_service_listing">
	
	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-check"></span>
		<span class="wcfm-page-heading-text"><?php _e( 'Applications', 'wcfm-custom-menus' ); ?></span>
		<?php do_action( 'wcfm_page_heading' ); ?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action( 'before_wcfm_service' ); ?>
		
		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('Applications', 'wcfm-custom-menus' ); ?></h2>
			<div class="wcfm-clearfix"></div>
	  </div>
	  <div class="wcfm-clearfix"></div><br />
		

	  <div class="wcfm-container">
			<div id="wwcfm_application_listing_expander" class="wcfm-content">
				<table id="wcfm-orders" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>                                                                                      
							<th><?php _e( 'ID', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Vendor', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Items', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Status', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Date', 'wc-frontend-manager' ); ?></th>
						</tr>
					</thead>
					<tfoot>
						<tr>                                                                                      
							<th><?php _e( 'ID', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Vendor', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Items', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Status', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Date', 'wc-frontend-manager' ); ?></th>
						</tr>
					</tfoot>
				</table>
				<div class="wcfm-clearfix"></div>
			</div>
		</div>
	
		<div class="wcfm-clearfix"></div>
		<?php
		do_action( 'after_wcfm_service' );
		?>
	</div>
</div>