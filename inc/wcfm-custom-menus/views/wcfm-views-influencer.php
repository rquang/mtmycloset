<?php
global $WCFM, $wp_query;
?>

<div class="collapse wcfm-collapse" id="wcfm_service_listing">

	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-check"></span>
		<span class="wcfm-page-heading-text">Become An Influencer</span>
		<?php do_action('wcfm_page_heading');?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action('before_wcfm_service');?>

		<div class="wcfm-container wcfm-top-element-container">
			<h2>Become An Influencer</h2>
		</div>
		<div class="wcfm-clearfix"></div><br />

		<div class="wcfm-container p-3">
			<?php
				$args = array(
					'post_type' => 'influencer',
					'post_status' => array('publish'),
					'meta_query' => array(
						array(
							'key' => 'user_id',
							'value' => get_current_user_id(),
							'compare' => 'LIKE',
						)
					)
				);
				$loop = new WP_Query($args);
				$count = $loop->found_posts;
				$ID = $loop->posts[0]->ID;
				$status = get_field('application_status', $ID);
				if (in_multi_array(7285, mt_get_vendor_group())) : ?>
					<div class="container">	
						<h3>Congratulations!</h3>
						<p>
							You've been accepted as a <strong>MT MyCloset Influencer</strong>.
						</p>
					</div>
				<?php
				elseif ($count == 0):
					get_template_part('/templates/influencer_application');
				else :
					if ($status == 'Pending') :
				?>
					<div class="container">
						<h3>Influencer Application Review in Progress</h3>
						<p>
							Your application is under review. We will contact to you with further details once we've reached a decision.
						</p>
					</div>
				<?php
					elseif ($status == 'Approve') :
				?>
					<div class="container">	
						<h3>Congratulations!</h3>
						<p>
							You've been accepted as a <strong>MT MyCloset Influencer</strong>.
						</p>
					</div>
				<?php
					else :
				?>
					<div class="container">
						<h3>Sorry, You're Application Has Been Declined</h3>
						<p>
							We have reviewed your application and it has been declined.
						</p>
						<p>
							You do not meet our qualifications and requirements to become an influencer at this time. Please try again at a later time.
						</p>
					</div>					
			<?php
					endif;		
				endif;
			?>
			<div class="wcfm-clearfix"></div>
			<?php
			do_action('after_wcfm_service');
			?>
		</div>
	</div>