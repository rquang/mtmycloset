<?php
global $WCFM, $wp_query;

?>

<div class="collapse wcfm-collapse" id="wcfm_service_listing">

	<div class="wcfm-page-headig">
		<span class="wcfmfa fa fa-check"></span>
		<span class="wcfm-page-heading-text"><?php _e('Applications', 'wcfm-custom-menus');?></span>
		<?php do_action('wcfm_page_heading');?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action('before_wcfm_service');?>

		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php _e('New Application', 'wcfm-custom-menus');?></h2>
		</div>
		<div class="wcfm-clearfix"></div><br />

		<div class="wcfm-container p-3">
			<?php
				if (mt_vendor_location_check() && mt_vendor_name_check() ) {
					if (in_array(mt_get_vendor_country(), array('CA', 'US'))) {
						get_template_part('/templates/sell_application');
					} else {
					?>
						<div class="container py-5">
							<h3>Applications Are Not Available in Your Region</h3>
							<p>
								We currently only accept applications in Canada and United States at this time.
							</p>
							<p>
								Thank You,
								<br>
								<b>MT MyCloset Team</b>
							</p>
						</div>						
					<?php
					}
				} else {
					if (!mt_vendor_location_check() ) {
					?>
						<div class="container py-5">
							<p>
								Please update your location information before submitting a sell application.
							</p>
							<a href="/dashboard/settings" class="primary-btn">Update Location Settings</a>
						</div>
					<?php
					}
					if (!mt_vendor_name_check() ) {
						?>
							<div class="container py-5">
								<p>
									Please update your personal information before submitting a sell application.
								</p>
								<a href="/dashboard/profile" class="primary-btn">Update Personal Settings</a>
							</div>
						<?php
					}
				}
			?>
			<div class="wcfm-clearfix"></div>
			<?php
			do_action('after_wcfm_service');
			?>
		</div>
	</div>