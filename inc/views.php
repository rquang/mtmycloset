<?php

function mt_currency_toggle()
{
    get_template_part('/templates/currency_toggle');
}

add_shortcode('mt_currency', 'mt_currency_toggle');

function mt_sell_application()
{
    get_template_part('/templates/sell_application');
}

add_shortcode('mt_sell_application', 'mt_sell_application');

// Redirect if not logged in for the sell application.
function mt_pre_sell_application()
{
    if (!is_singular()) {
        return;
    }

    global $post;
    if (!empty($post->post_content)) {
        $regex = get_shortcode_regex();
        preg_match_all('/' . $regex . '/', $post->post_content, $matches);
        if (!empty($matches[2]) && in_array('mt_sell_application', $matches[2]) && is_user_logged_in()) {
            // redirect to third party site
        } else {
            wp_redirect(home_url('sell'));
            exit;
        }
    }
}
// add_action('template_redirect', 'mt_pre_sell_application', 1);

function mt_sizing_chart()
{
    $product_slug = get_queried_object()->post_name;
    $product = get_page_by_path($product_slug, OBJECT, 'product');
    $id = $product->ID;
    $vendor_id = get_post_field( 'post_author', $id );
    $terms = get_the_terms($id, 'product_cat');
    $gender;

    if ($terms && !is_wp_error($terms)) {
        foreach ($terms as $term) {
            if ($term->parent == 186 || $term->term_id == 186) {
                $gender = 'mens';
                break;
            } else if ($term->parent == 229 || $term->term_id == 229) {
                $gender = 'womens';
                break;
            }
        }
    }

    ob_start();

    if (in_multi_array(138, mt_get_product_vendor_group())) {
        echo('<div class="mt-boutique_size_chart>');
        the_field('size_chart', 'user_' . $vendor_id);
        echo('</div>');
    } else if ($gender == 'mens') {
        // get_template_part('/templates/sizing/gender', 'mens');
        ?>
<h2>Men's Sizing</h2>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tops
        (Includes: Formal Indian Wear, Jackets, Suit Jackets, Shirts, etc.)<span
            class="Apple-converted-space"> </span></span><em
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><br
                style="box-sizing: border-box;" /></u></em></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Chest
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                42 - 44</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                46 - 48</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                54 - 56</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span></p>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Pants
        (Includes: Dress Pants, Chinos, Jeans, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Waist
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                25 - 28</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 35</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                36 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; vertical-align: baseline; font-weight: inherit !important; font-size: inherit !important; line-height: inherit !important; font-family: inherit !important; margin: 0px 0px inherit !important 0px;">42 +</span><u
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"></u>
            </td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span><br style="box-sizing: border-box;" /><br
        style="box-sizing: border-box;" /></p>

        <?php
} else if ($gender == 'womens') {
        // get_template_part('/templates/sizing/gender', 'womens');
        ?>
<h2>Women's Sizing</h2>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tops
        (Includes: Formal Indian Wear, Shirts, Dresses, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 34</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35 - 37</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41 - 43</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                44 - 46</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                47 - 49</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span><span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><br
            style="box-sizing: border-box;" /></span></p>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Casual
        Wear (Includes: Dresses, Skirts, Pants, etc.)</span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (Inches)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                29 - 31</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32 - 34</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35 - 37</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                38 - 40</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41 - 43</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                44 - 46</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                47 - 49</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                50 - 52</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note: Sizes
                may vary depending on the brand of the product</u></em></span></p>

<table
    style="box-sizing: border-box; border-spacing: 0px; max-width: 100%; margin: 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; width: 710px; color: #000000; border-collapse: collapse !important;">
    <tbody
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Size</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">US</span>
            </td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                <span
                    style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Bust
                    (in)</span></td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XXS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                0</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                31.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XS</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                32.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                S</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                4</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                33.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                M</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                6</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                35.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                L</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                8</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                37.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                10</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                39.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                2 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                12</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                41.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                3 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                14</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                43.5</td>
        </tr>
        <tr
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                4 XL</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                16</td>
            <td
                style="box-sizing: border-box; margin: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; line-height: inherit; vertical-align: top; padding: 5px !important; border: 1px solid #dddddd !important; float: none !important;">
                46.5</td>
        </tr>
    </tbody>
</table>
<p
    style="box-sizing: border-box; padding: 0px; border: 0px; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Roboto, helvetica, arial, sans-serif; vertical-align: baseline; color: #000000; margin: 0px 0px 5px !important 0px;">
    <span
        style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><em
            style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;"><u
                style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Note:
                Sizes may vary depending on the brand of the product</u></em></span></p>

        <?php
}
    return ob_get_clean();
}

add_shortcode('mt_sizing_chart', 'mt_sizing_chart');
