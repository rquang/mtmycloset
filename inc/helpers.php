<?php

function mt_get_closet_id($ID)
{
    $args = array(
        'post_type' => 'closet',
        'orderby' => 'publish_date',
        'order' => 'ASC',
        'posts_per_page' => 1,
        'meta_query' => array(
            array(
                'key' => 'user_id',
                'value' => $ID,
                'compare' => 'LIKE',
            ),
        ),
    );
    $loop = new WP_Query($args);
    $closet = $loop->posts[0];
    wp_reset_postdata();
    return $closet->ID;
}

function mt_get_total_likes($ID)
{
    global $wpdb;
    $likes = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}yith_wcwl WHERE prod_id = $ID");
    return $likes;
}

function mt_get_total_closet_items($ID)
{
    $loop = mt_get_closet_items($ID);
    return $loop->post_count;
}

function mt_get_total_closet_items_sold($ID)
{
    $count = 0;
    $loop = mt_get_closet_items($ID);
    if ($loop->have_posts()):
        foreach ($loop->get_posts() as $p):
            $product = wc_get_product($p->ID);
            if ($product->get_total_sales() > 0) {
                ++$count;
            }

        endforeach;
    endif;
    return $count;
}

function mt_get_closet_items($ID, $count = -1)
{
    $args = array(
        'post_type' => 'product',
        'orderby' => 'publish_date',
        'order' => 'ASC',
        'posts_per_page' => $count,
        'meta_query' => array(
            array(
                'key' => 'closet_id',
                'value' => $ID,
                'compare' => 'LIKE',
            ),
        ),
    );
    $loop = new WP_Query($args);
    wp_reset_postdata();
    return $loop;
}

function mt_get_total_closet_followers($ID)
{
    $followers = get_field('closet_followers', $ID);
    if (is_countable($followers)) {
        return count($followers);
    } else {
        return 0;
    }

}

function mt_get_total_closet_following($ID)
{
    $following = get_field('closet_following', $ID);
    if (is_countable($following)) {
        return count($following);
    } else {
        return 0;
    }

}

function mt_get_closet_name()
{
    $user_id = get_field('closet_id');
    return $user_id['display_name'];
}

/**
 * Recursively get taxonomy and its children
 *
 * @param string $taxonomy
 * @param int $parent - parent term id
 * @return array
 */
function get_taxonomy_hierarchy($taxonomy, $parent = 0)
{
    // only 1 taxonomy
    $taxonomy = is_array($taxonomy) ? array_shift($taxonomy) : $taxonomy;
    // get all direct decendants of the $parent
    $terms = get_terms($taxonomy, array('parent' => $parent));
    // prepare a new array.  these are the children of $parent
    // we'll ultimately copy all the $terms into this new array, but only after they
    // find their own children
    $children = array();
    // go through all the direct decendants of $parent, and gather their children
    foreach ($terms as $term) {
        // recurse to get the direct decendants of "this" term
        $term->children = get_taxonomy_hierarchy($taxonomy, $term->term_id);
        // add the term to our new array
        $children[$term->term_id] = $term;
    }
    // send the results back to the caller
    return $children;
}

function mt_publish_items($ID)
{
    $args = array(
        'post_type' => 'item',
        'orderby' => 'publish_date',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'application_id',
                'value' => $ID,
                'compare' => 'LIKE',
            ),
        ),
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()):
        foreach ($loop->get_posts() as $item):
            $ID = $item->ID;
            $status = get_field('item_status', $ID);

            // if item is not approved, skip
            if ($status != 'Approved') {
                continue;
            }

            // vars
            $vendor_id = get_field('user_id');
            // $type = get_field('type', $ID)['type'];
            // $formal = get_field('type', $ID)['formal'];
            // $image = get_field('photos_clothing', $ID)['front'];
            $category = get_field('type', $ID)['category']->term_id;
            $size = get_field('information', $ID)['size']->term_id;
            $name = get_field('product_name', $ID)['product_name'];
            $description = get_field('description', $ID)['description'];
            $weight = get_field('information', $ID)['weight'];
            $designer = get_field('information', $ID)['designer']->term_id;
            $color = get_field('information', $ID)['color']->term_id;
            // $retail_price = get_field('pricing', $ID)['retail_price'];
            if ($suggested_price > 0):
                $listing_price = get_field('suggested_price', $ID);
            else:
                $listing_price = get_field('pricing', $ID)['listing_price'];
            endif;
            $commission = get_field('commission', $ID);

            // Create a new product
            $post = array(
                'post_type' => 'product',
                'post_content' => $description,
            );
            if ($name) {
                $post['post_status'] = 'publish';
                $post['post_title'] = $name;                
            } else {
                $post['post_status'] = 'pending';
                $post['post_title'] = 'Product Name Required';
            }
            // insert new item
            $post_id = wp_insert_post($post);

            // update fields

            // get image ID for URL
            // $image_id = attachment_url_to_postid($image['url']);
            // LIVE
            $image_id = attachment_url_to_postid('https://mtmycloset.com/wp-content/uploads/2019/07/image-coming-soon.jpg');
            // DEV
            // $image_id = attachment_url_to_postid('http://mtmc.local/wp-content/uploads/2019/07/image-coming-soon.jpg');
            update_post_meta($post_id, '_thumbnail_id', $image_id);

            update_post_meta($post_id, '_regular_price', $listing_price);
            update_post_meta($post_id, '_price', $listing_price);
            update_post_meta($post_id, '_manage_stock', 'yes');
            wc_update_product_stock($post_id, 1, 'set');
            // Convert lbs to kg
            update_post_meta($post_id, '_weight', $weight * 0.453592);

            // update WCFM fields
            mt_wcfm_publish_item($post_id, $vendor_id, $commission);

            // update taxonomies
            wp_set_object_terms($post_id, $category, 'product_cat');
            wp_set_object_terms($post_id, $designer, 'pa_designer');
            wp_set_object_terms($post_id, $color, 'pa_color');
            wp_set_object_terms($post_id, $size, 'pa_size');

            // // update ACF fields
            // $args = array(
            //     'type' => $type,
            //     'formal' => $formal,
            // );
            // update_field('type', $args, $post_id);

        endforeach;
    endif;
}

function mt_wcfm_publish_item($product_id, $vendor_id, $commission)
{
    global $WCFM, $WCFMmp;

    $wcfmmp_store = $vendor_id;
    if ($wcfmmp_store) {
        $arg = array(
            'ID' => $product_id,
            'post_author' => $wcfmmp_store,
        );
        wp_update_post($arg);

        $WCFMmp->wcfmmp_vendor->wcfmmp_reset_vendor_taxonomy($wcfmmp_store, $product_id);

        // Update vendor category list
        $pcategories = get_the_terms($product_id, 'product_cat');
        if (!empty($pcategories)) {
            foreach ($pcategories as $pkey => $pcategory) {
                $WCFMmp->wcfmmp_vendor->wcfmmp_save_vendor_taxonomy($wcfmmp_store, $product_id, $pcategory->term_id);
            }
        }
    } else {
        $arg = array(
            'ID' => $product_id,
            'post_author' => get_current_user_id(),
        );
        wp_update_post($arg);
    }

    // Update Product Commission
    $arg = array(
        'commission_mode' => 'percent',
        'commission_percent' => $commission,
    );

    update_post_meta($product_id, '_wcfmmp_commission', $arg);
}

/**
 * Attach images to product (feature/ gallery)
 */
function attach_product_thumbnail($post_id, $url, $flag)
{
    /*
     * If allow_url_fopen is enable in php.ini then use this
     */
    $image_url = $url;
    $url_array = explode('/', $url);
    $image_name = $url_array[count($url_array) - 1];
    $image_data = file_get_contents($image_url); // Get image data
    /*
     * If allow_url_fopen is not enable in php.ini then use this
     */
    // $image_url = $url;
    // $url_array = explode('/',$url);
    // $image_name = $url_array[count($url_array)-1];
    // $ch = curl_init();
    // curl_setopt ($ch, CURLOPT_URL, $image_url);
    // // Getting binary data
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    // $image_data = curl_exec($ch);
    // curl_close($ch);
    $upload_dir = wp_upload_dir(); // Set upload folder
    $unique_file_name = wp_unique_filename($upload_dir['path'], $image_name); //    Generate unique name
    $filename = basename($unique_file_name); // Create image file name
    // Check folder permission and define file location
    if (wp_mkdir_p($upload_dir['path'])) {
        $file = $upload_dir['path'] . '/' . $filename;
    } else {
        $file = $upload_dir['basedir'] . '/' . $filename;
    }
    // Create the image file on the server
    file_put_contents($file, $image_data);
    // Check image file type
    $wp_filetype = wp_check_filetype($filename, null);
    // Set attachment data
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit',
    );
    // Create the attachment
    $attach_id = wp_insert_attachment($attachment, $file, $post_id);
    // Include image.php
    require_once ABSPATH . 'wp-admin/includes/image.php';
    // Define attachment metadata
    $attach_data = wp_generate_attachment_metadata($attach_id, $file);
    // Assign metadata to attachment
    wp_update_attachment_metadata($attach_id, $attach_data);
    // asign to feature image
    if ($flag == 0) {
        // And finally assign featured image to post
        set_post_thumbnail($post_id, $attach_id);
    }
    // assign to the product gallery
    if ($flag == 1) {
        // Add gallery image to product
        $attach_id_array = get_post_meta($post_id, '_product_image_gallery', true);
        $attach_id_array .= ',' . $attach_id;
        update_post_meta($post_id, '_product_image_gallery', $attach_id_array);
    }
}

function mt_redirect($url)
{
    $baseUri = get_site_url();

    if (headers_sent()) {
        $string = '<script type="text/javascript">';
        $string .= 'window.location = "' . $baseUri . $url . '"';
        $string .= '</script>';

        echo $string;
    } else {
        if (isset($_SERVER['HTTP_REFERER']) and ($url == $_SERVER['HTTP_REFERER'])) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        } else {
            header('Location: ' . $baseUri . $url);
        }

    }
    exit;
}

// Function to get the client ip address
function get_client_ip_server() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
 
    return trim($ipaddress);
}

// Get Country from IP
function get_client_country() {
    $ipAddress = get_client_ip_server();
    write_log($ipAddress);
    $details = json_decode(file_get_contents("http://ipinfo.io/{$ipAddress}/json"));
    write_log($details->country);
    return $details->country;
}

function in_multi_array($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_multi_array($needle, $item, $strict))) {
            return true;
        }
    }
    return false; 
}

function mt_product_check() {
    $args = array(
        'post_type' => 'product',
        'post_status' => array('publish'),
        'posts_per_page' => -1
    );
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ) :
        $closets = [];
        foreach ($loop->get_posts() as $p) :
            $product = wc_get_product($p->ID);
            $closet_id = get_userdata($p->post_author)->id;
            if (mt_get_vendor_group($closet_id)[0][0] == 138) {
                continue;
            }
            $start_date = $product->get_date_created()->format('Y-m-d');
            $end_date = current_time('Y-m-d');
            $date_diff = floor((strtotime($start_date) - strtotime($end_date)) / 3600 / 24 * -1);
            $closets[$closet_id] = $date_diff;
        endforeach;
        foreach ($closets as $key => $value) {
            switch ($value) {
                case 14:
                    cm_send_14_day($key);
                    break;
                case 30:
                    cm_send_30_day($key);
                    break;
                case 60:
                    cm_send_60_day($key);
                    break;
                case 90:
                    cm_send_90_day($key);
                    break;
            }
        }
    endif;
    wp_reset_postdata();
}

function searchMultiArray($needle, $haystack){
    $needle = trim($needle);
    if(!is_array($haystack))
        return False;

    foreach($haystack as $key=>$value){
        if(is_array($value)){
            if(searchMultiArray($needle, $value))
                return True;
            else
               searchMultiArray($needle, $value);
        }
        else
        if(trim($value) === trim($needle)){
            return True;
        }
    }

    return False;
}