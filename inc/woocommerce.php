<?php

// /**
//  * Overwrite product_tag taxonomy properties to effectively hide it from WP admin ..
//  */
// add_action('init', function () {
//     register_taxonomy('product_cat', 'product', [
//         'public' => false,
//         'show_ui' => false,
//         'show_admin_column' => false,
//         'show_in_nav_menus' => false,
//         'show_tagcloud' => false,
//     ]);
// }, 100);

// /**
//  * .. and also remove the column from Products table - it's also hardcoded there.
//  */
// add_action('admin_init', function () {
//     add_filter('manage_product_posts_columns', function ($columns) {
//         unset($columns['product_cat']);
//         return $columns;
//     }, 100);
// });

function remove_linked_products($tabs)
{

    // unset($tabs['general']);

    //  unset($tabs['inventory']);

    //  unset($tabs['shipping']);

    unset($tabs['linked_product']);

    // unset($tabs['attribute']);

    unset($tabs['advanced']);

    return ($tabs);

}

add_filter('woocommerce_product_data_tabs', 'remove_linked_products', 10, 1);

function my_remove_product_type_options($options)
{
    if (isset($options['virtual'])) {
        unset($options['virtual']);
    }
    if (isset($options['downloadable'])) {
        unset($options['downloadable']);
    }
    return $options;
}
add_filter('product_type_options', 'my_remove_product_type_options');

add_action('woocommerce_single_product_summary', 'mt_single_product_size', 6);
function mt_single_product_size()
{
    global $product;

    $product_id = $product->get_id(); // The product ID

    $terms = get_the_terms($product_id, 'size');

    if ($terms && !is_wp_error($terms)) {

        foreach ($terms as $term) {
            $draught_links[] = $term->name;
            // Displaying your custom field under the title
            echo '<h6 class="product-size">' . $term->name . '</h6>';
        }

    }
}

// change roles to store vendor
function my_new_customer_data($new_customer_data)
{
    $new_customer_data['role'] = 'wcfm_vendor';
    return $new_customer_data;
}
add_filter('woocommerce_new_customer_data', 'my_new_customer_data');

add_filter('woocommerce_account_menu_items', 'mt_rename_address_my_account', 999);
function mt_rename_address_my_account($items)
{
    $items['orders'] = 'Purchase History';
    return $items;
}

/*
 * Create a default Product Attribute object for the supplied name
 *
 * @param  string   name        Product Attribute taxonomy name
 *
 * @return WC_Product_Attribute/bool  new Attribute or false if named Attribute is not found
 *
 */
function acme_make_product_attribute($name)
{
    global $wc_product_attributes;
    if (isset($wc_product_attributes[$name])) {
        $newattr = new WC_Product_Attribute();
        $newattr->set_id(1); //any positive value is interpreted as is_taxonomy=true
        $newattr->set_name($name);
        $newattr->set_visible(true);
        $newattr->set_variation(false);
        //example of setting default value for item
        if ($name == 'pa_brand') {
            $term = get_term_by('slug', 'acme', $name);
            $newattr->set_options(array($term->term_id));
        }
        return $newattr;
    } else {
        return false;
    }
}

/*
 * Add default attributes to a product
 */
function acme_default_product_attributes()
{
    global $product;
    if (!$product) {
        $product = $GLOBALS['product_object'];
    }
    if (!$product) {
        return;
    }
    $attributes = $product->get_attributes();

    $defaultAttributes = array(
        'pa_designer',
        'pa_size-tops-outerwear',
        'pa_size-bottom-pants',
        'pa_size-footwear',
        'pa_size-accessories',
    );

    $changed = false;
    foreach ($defaultAttributes as $key) {
        if (!isset($attributes[$key])) {
            $newattr = acme_make_product_attribute($key);
            if ($newattr) {
                $attributes[$key] = $newattr;
            }
            $changed = true;
        }
    }
    if ($changed) {
        $product->set_attributes($attributes);
    }
}
/*
 * added to last hook before rendering of Product Edit screen
 */
add_action('woocommerce_product_write_panel_tabs', 'acme_default_product_attributes');

// Show Designer Under Title in Shop Loop

add_action('woocommerce_after_shop_loop_item_title', 'designer_after_title');
function designer_after_title()
{

    global $product;

    // Check that we got the instance of the WC_Product object, to be sure (can be removed)
    if (!is_object($product)) {
        $product = wc_get_product(get_the_id());
    }

    ?>
        <div class="mt-attributes">
            <?php if ($product->get_attribute('pa_designer')): ?>
                <a class="mt-product-designer" href="<?php echo get_term_link(sanitize_title($product->get_attribute('pa_designer')), 'pa_designer'); ?>"><?php echo $product->get_attribute('pa_designer'); ?></a>
            <?php endif;?>
            <span class="mt-product-size">
                <?php
if ($product->get_attribute('pa_size-accessories')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-accessories') . '</b>';
    } else if ($product->get_attribute('pa_size-tops-outerwear')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-tops-outerwear') . '</b>';
    } else if ($product->get_attribute('pa_size-bottoms-pants')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-bottoms-pants') . '</b>';
    } else if ($product->get_attribute('pa_size-footwear')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-footwear') . '</b>';
    }
    ?>
            </span>
        </div>
    <?php

}

add_action('save_post_product', 'mt_save_product_post', 10, 3);

function mt_save_product_post($postID, $post, $update)
{
    if (!$update) {
        //  $update is false if we're creating a new post
        update_post_meta($post->ID, '_manage_stock', 'yes');
        update_post_meta($post->ID, '_stock', '1');
    }
}

add_action('woocommerce_single_product_summary', 'mt_action_after_single_product_title', 6);
function mt_action_after_single_product_title()
{
    global $product;
    ?>

    <div class="mt-single-attributes">
    <?php if ($product->get_attribute('pa_designer')): ?>
        <a class="mt-product-designer" href="<?php echo get_term_link(sanitize_title($product->get_attribute('pa_designer')), 'pa_designer'); ?>"><?php echo $product->get_attribute('pa_designer'); ?></a>
    <?php endif;?>
    <p class="mt-product-size">
    <?php
if ($product->get_attribute('pa_size-accessories')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-accessories') . '</b>';
    } else if ($product->get_attribute('pa_size-tops-outerwear')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-tops-outerwear') . '</b>';
    } else if ($product->get_attribute('pa_size-bottoms-pants')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-bottoms-pants') . '</b>';
    } else if ($product->get_attribute('pa_size-footwear')) {
        echo 'Size: <b>' . $product->get_attribute('pa_size-footwear') . '</b>';
    }
    ?>
    </p>
    </div>

    <?php if (have_rows('clothing_details') || have_rows('size_details') || have_rows('additional_notes')): ?>
        <div class="mt-single-product-details">
            <table class="mt-single-product-details-table">
                <?php if (have_rows('clothing_details')): ?>
                    <tbody>
                        <tr>
                            <td colspan="2" class="mt-single-product-details-title">Clothing Details</td>
                        </tr>
                        <?php while (have_rows('clothing_details')): the_row();?>
																						                                <tr>
																						                                    <td class="mt-single-product-details-descriptor"><?php the_sub_field('Descriptor');?></td>
																						                                    <td class="mt-single-product-details-value"><?php the_sub_field('value');?></td>
																						                                </tr>
																						                            <?php endwhile;?>
                    </tbody>
                <?php endif;?>
                <?php if (have_rows('size_details')): ?>
                    <tbody>
                        <tr>
                            <td colspan="2" class="mt-single-product-details-title">Size Details</td>
                        </tr>
                        <?php while (have_rows('size_details')): the_row();?>
																						                                <tr>
																						                                    <td class="mt-single-product-details-descriptor"><?php the_sub_field('Descriptor');?></td>
																						                                    <td class="mt-single-product-details-value"><?php the_sub_field('value');?></td>
																						                                </tr>
																						                            <?php endwhile;?>
                    </tbody>
                <?php endif;?>
                <?php if (have_rows('additional_notes')): ?>
                    <tbody>
                        <tr>
                            <td colspan="2" class="mt-single-product-details-title">Additional Notes</td>
                        </tr>
                        <?php while (have_rows('additional_notes')): the_row();?>
																						                                <tr>
																						                                    <td class="mt-single-product-details-descriptor"><?php the_sub_field('Descriptor');?></td>
																						                                    <td class="mt-single-product-details-value"><?php the_sub_field('value');?></td>
																						                                </tr>
																						                            <?php endwhile;?>
                    </tbody>
                <?php endif;?>
                <?php if (have_rows('damages')): ?>
                    <tbody>
                        <tr>
                            <td colspan="2" class="mt-single-product-details-title">Damages</td>
                        </tr>
                        <?php while (have_rows('damages')): the_row();?>
																						                                <tr>
																						                                    <td class="mt-single-product-details-descriptor"><?php the_sub_field('Descriptor');?></td>
																						                                    <td class="mt-single-product-details-value"><?php the_sub_field('value');?></td>
																						                                </tr>
																						                            <?php endwhile;?>
                    </tbody>
                <?php endif;?>
            </table>
        </div>
    <?php endif;?>

    <?php
}

/**
 * Replace 'customer' role (WooCommerce use by default) with your own one.
 **/
add_filter('woocommerce_new_customer_data', 'wc_assign_custom_role', 10, 1);

function wc_assign_custom_role($args)
{
    $args['role'] = 'wcfm_vendor';

    return $args;
}

// add_filter('wc_social_login_facebook_new_user_data', 'mt_assign_social_login_role', 1, 2);
// add_filter('wc_social_login_twitter_new_user_data', 'mt_assign_social_login_role', 1, 2);
// add_filter('wc_social_login_google_new_user_data', 'mt_assign_social_login_role', 1, 2);

function mt_assign_social_login($args, $profile)
{
    $args = array(
        'role' => 'wcfm_vender',
        'user_login' => $profile->has_email() ? substr($email, 0, strrpos(sanitize_email($profile->get_email()), '@')) : $profile->get_username(),
        'user_email' => $profile->get_email(),
        'user_pass' => wp_generate_password(),
        'first_name' => $profile->get_first_name(),
        'last_name' => $profile->get_last_name(),
    );
    return $args;
}

// function addPriceSuffix($format, $currency_pos) {
// 	switch ( $currency_pos ) {
// 		case 'left' :
// 			$currency = get_woocommerce_currency();
// 			$format = '%1$s%2$s&nbsp;' . $currency;
// 		break;
// 	}
 
// 	return $format;
// }
 
// function addPriceSuffixAction() {
// 	add_action('woocommerce_price_format', 'addPriceSuffix', 1, 2);
// }
 
// add_action('woocommerce_before_cart', 'addPriceSuffixAction');
// add_action('woocommerce_review_order_before_order_total', 'addPriceSuffixAction');

function mt_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'USD':
            $currency_symbol = 'USD $';
            break;
        case 'CAD':
            $currency_symbol = 'CAD $';
            break;
    }
    return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'mt_currency_symbol', 30, 2);

/**
 * Allow Shop Managers to edit and promote users with the Editor role 
 * using the 'woocommerce_shop_manager_editable_roles' filter.
 *
 * @param array $roles Array of role slugs for users Shop Managers can edit.
 * @return array
 */
function myextension_shop_manager_role_edit_capabilities( $roles ) {
    $roles[] = 'wcfm_vendor';
    return $roles;
}
add_filter( 'woocommerce_shop_manager_editable_roles', 'myextension_shop_manager_role_edit_capabilities' );

// add_action( 'woocommerce_before_single_product_summary', 'mt_yith_wapo' );
add_action( 'woocommerce_before_add_to_cart_quantity', 'mt_yith_wapo' );

function mt_yith_wapo () {
    global $product;
    $vendor = get_user_by( 'id', $product->post->post_author );
    if (mt_get_vendor_group($vendor->ID)[0][0] != 138) {
        echo do_shortcode('[yith_wapo_show_options]');
    }
}