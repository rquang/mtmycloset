<?php
get_header();

// Default to single post
// Get the grosso custom options
$grosso_page_options = get_post_custom(get_the_ID());

$grosso_show_title_page = 'yes';
$grosso_show_breadcrumb = 'yes';
$grosso_featured_slider = 'none';
$grosso_subtitle = '';
$grosso_show_title_background = 0;
$grosso_title_background_image = '';
$grosso_title_alignment = 'left_title';

if (isset($grosso_page_options['grosso_show_title_page']) && trim($grosso_page_options['grosso_show_title_page'][0]) != '') {
	$grosso_show_title_page = $grosso_page_options['grosso_show_title_page'][0];
}

if (isset($grosso_page_options['grosso_show_breadcrumb']) && trim($grosso_page_options['grosso_show_breadcrumb'][0]) != '') {
	$grosso_show_breadcrumb = $grosso_page_options['grosso_show_breadcrumb'][0];
}

if (isset($grosso_page_options['grosso_rev_slider']) && trim($grosso_page_options['grosso_rev_slider'][0]) != '') {
	$grosso_featured_slider = $grosso_page_options['grosso_rev_slider'][0];
}


if (isset($grosso_page_options['grosso_page_subtitle']) && trim($grosso_page_options['grosso_page_subtitle'][0]) != '') {
	$grosso_subtitle = $grosso_page_options['grosso_page_subtitle'][0];
}

if (isset($grosso_page_options['grosso_title_background_imgid']) && trim($grosso_page_options['grosso_title_background_imgid'][0]) != '') {
	$grosso_img = wp_get_attachment_image_src($grosso_page_options['grosso_title_background_imgid'][0], 'full');
	$grosso_title_background_image = $grosso_img[0];
}

if (isset($grosso_page_options['grosso_title_alignment']) && trim($grosso_page_options['grosso_title_alignment'][0]) != '') {
	$grosso_title_alignment = $grosso_page_options['grosso_title_alignment'][0];
}
?>
<div id="content" <?php if (!empty($grosso_sidebar_classes)) echo 'class="' . esc_attr(implode(' ', $grosso_sidebar_classes)) . '"'; ?> >
	<?php while (have_posts()) : the_post(); ?>
		<?php if ($grosso_show_title_page == 'yes' || $grosso_show_breadcrumb == 'yes'): ?>
			<div id="grosso_page_title" class="grosso_title_holder <?php echo esc_attr($grosso_title_alignment) ?> <?php if ($grosso_title_background_image): ?>title_has_image<?php endif; ?>">
				<?php if ($grosso_title_background_image): ?><div class="grosso-zoomable-background" style="background-image: url('<?php echo esc_url($grosso_title_background_image) ?>');"></div><?php endif; ?>
				<div class="inner fixed">
					<!-- BREADCRUMB -->
					<?php if ($grosso_show_breadcrumb == 'yes'): ?>
						<?php grosso_breadcrumb() ?>
					<?php endif; ?>
					<!-- END OF BREADCRUMB -->
					<?php if ($grosso_show_title_page == 'yes'): ?>
						<h1	class="heading-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h1>
						<?php if ($grosso_subtitle): ?>
                            <h6><?php echo esc_html($grosso_subtitle) ?></h6>
						<?php endif; ?>
					<?php endif; ?>
                    <?php get_template_part( 'partials/blog-post-meta-bottom' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="inner">
			<!-- CONTENT WRAPPER -->
			<div id="main" class="fixed box box-common">
				<div class="content_holder">
					<?php get_template_part('content', get_post_format()); ?>
					<?php
					if (comments_open() || get_comments_number()) :
						comments_template('', true);
					endif;
					?>
				</div>

				<div class="clear"></div>
				<?php if (function_exists('grosso_share_links')): ?>
					<?php grosso_share_links(get_the_title(), get_permalink()); ?>
				<?php endif; ?>
			</div>

            <!-- Previous / Next links -->
			<?php if (grosso_get_option('show_prev_next')): ?>
				<?php echo grosso_post_nav(); ?>
			<?php endif; ?>
		</div>
		<!-- END OF CONTENT WRAPPER -->
	<?php endwhile; // end of the loop.    ?>
</div>
<?php
get_footer();
