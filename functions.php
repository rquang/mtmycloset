<?php
/*
Plugin Name:    MT My Closet
Plugin URI:        https://mtmycloset.com
Description:    Custom plugin for MT My Closet.
Version:        1.0.0
Author:            Ryan Quang
Author URI:        https://socialhero.ca
License:        GPL-2.0+
License URI:    http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
 */

if (!defined('WPINC')) {
    die;
}

// include all files within inc directory
foreach (glob(get_stylesheet_directory() . "/inc/*.php") as $function) {
    $function = basename($function);
    require_once get_stylesheet_directory() . '/inc/' . $function;
}

// Writes to the Debug Log
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

add_action('wp_enqueue_scripts', 'grosso_child_enqueue_styles');
function grosso_child_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );

    if (is_rtl()) {
        wp_enqueue_style('parent-rtl', get_template_directory_uri() . '/rtl.css');
        wp_enqueue_style('child-rtl',
            get_stylesheet_directory_uri() . '/rtl.css',
            array('parent-rtl')
        );
    }

    wp_enqueue_script('child-grosso-front',
        get_stylesheet_directory_uri() . '/assets/js/grosso-front.js',
        array('grosso-front'),
        false,
        true
    );
}

add_action('wp_enqueue_scripts', 'custom_enqueue_files');
/**
 * Loads <list assets here>.
 */
function custom_enqueue_files()
{
    // if this is not the front page, abort.
    // if ( ! is_front_page() ) {
    //     return;
    // }

    // loads a CSS file in the head.
    wp_enqueue_style('theme-css', get_stylesheet_directory_uri() . '/assets/css/theme.css', array(), filemtime(get_stylesheet_directory() . '/style.css'));

    // loads JS files in the footer.
    // wp_enqueue_script('sell-js', get_stylesheet_directory_uri() . '/assets/js/sell.min.js', array('jquery'), null, true);
    if (strpos($_SERVER['REQUEST_URI'], 'dashboard') !== false) {
        wp_enqueue_script('dashboard-js', get_stylesheet_directory_uri() . '/assets/js/dashboard.min.js', array('jquery'), null, true);
    }
    if (strpos($_SERVER['REQUEST_URI'], 'faq') !== false) {
        wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), null, true);
    }

}

// //create a new closet and attach to user on new registrations.
// add_action('user_register', 'mt_new_registration', 10, 1);

// function mt_new_registration($user_id)
// {

//     $user = get_userdata($user_id);

//     $post = array(
//         'post_type' => 'closet', // Your post type ( post, page, custom post type )
//         'post_status' => 'publish', // (publish, draft, private, etc.)
//         'post_title' => wp_strip_all_tags($user->data->display_name),
//     );
//     // insert the post
//     $post_id = wp_insert_post($post);

//     update_field('user_id', $user_id, $post_id);
// }

function mt_update_all_users_to_closet() {
    $mtargs = array(
        'exclude'      => array(12,13,792,871,741,804)
    ); 
    $mtusers = get_users($mtargs);
    foreach ($mtusers as $mtuser) {
        delete_user_meta( $mtuser->ID, '_wcfm_vendor_group' );
        mt_update_vendor_membership($mtuser->ID,24,6963);
    }
}

// mt_update_all_users_to_closet();