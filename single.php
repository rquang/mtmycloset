<?php
get_header();

// Default to single post
// Get the grosso custom options
$grosso_page_options = get_post_custom(get_the_ID());

$grosso_show_title_page = 'yes';
$grosso_show_breadcrumb = 'yes';
$grosso_featured_slider = 'none';
$grosso_subtitle = '';
$grosso_show_title_background = 0;
$grosso_title_background_image = '';
$grosso_title_alignment = 'left_title';

if (isset($grosso_page_options['grosso_show_title_page']) && trim($grosso_page_options['grosso_show_title_page'][0]) != '') {
	$grosso_show_title_page = $grosso_page_options['grosso_show_title_page'][0];
}

if (isset($grosso_page_options['grosso_show_breadcrumb']) && trim($grosso_page_options['grosso_show_breadcrumb'][0]) != '') {
	$grosso_show_breadcrumb = $grosso_page_options['grosso_show_breadcrumb'][0];
}

if (isset($grosso_page_options['grosso_rev_slider']) && trim($grosso_page_options['grosso_rev_slider'][0]) != '') {
	$grosso_featured_slider = $grosso_page_options['grosso_rev_slider'][0];
}


if (isset($grosso_page_options['grosso_page_subtitle']) && trim($grosso_page_options['grosso_page_subtitle'][0]) != '') {
	$grosso_subtitle = $grosso_page_options['grosso_page_subtitle'][0];
}

if (isset($grosso_page_options['grosso_title_background_imgid']) && trim($grosso_page_options['grosso_title_background_imgid'][0]) != '') {
	$grosso_img = wp_get_attachment_image_src($grosso_page_options['grosso_title_background_imgid'][0], 'full');
	$grosso_title_background_image = $grosso_img[0];
}

if (isset($grosso_page_options['grosso_title_alignment']) && trim($grosso_page_options['grosso_title_alignment'][0]) != '') {
	$grosso_title_alignment = $grosso_page_options['grosso_title_alignment'][0];
}

?>
<div id="content" <?php if (!empty($grosso_sidebar_classes)) echo 'class="' . esc_attr(implode(' ', $grosso_sidebar_classes)) . '"'; ?> >
	<?php while (have_posts()) : the_post(); ?>
		<?php if ($grosso_show_title_page == 'yes' || $grosso_show_breadcrumb == 'yes'): ?>
			<div id="grosso_page_title" class="grosso_title_holder <?php echo esc_attr($grosso_title_alignment) ?> <?php if ($grosso_title_background_image): ?>title_has_image<?php endif; ?>">
				<?php if ($grosso_title_background_image): ?><div class="grosso-zoomable-background" style="background-image: url('<?php echo esc_url($grosso_title_background_image) ?>');"></div><?php endif; ?>
				<div class="inner fixed">
					<!-- BREADCRUMB -->
					<?php if ($grosso_show_breadcrumb == 'yes'): ?>
						<?php grosso_breadcrumb() ?>
					<?php endif; ?>
					<!-- END OF BREADCRUMB -->
					<?php if ($grosso_show_title_page == 'yes'): ?>
						<h1	class="heading-title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h1>
						<?php if ($grosso_subtitle): ?>
                            <h6><?php echo esc_html($grosso_subtitle) ?></h6>
						<?php endif; ?>
					<?php endif; ?>
                    <?php get_template_part( 'partials/blog-post-meta-bottom' ); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="inner">
			<!-- CONTENT WRAPPER -->
			<div id="main" class="fixed box box-common">
				<div class="content_holder">
					<?php get_template_part('content', get_post_format()); ?>
					<?php
					if (comments_open() || get_comments_number()) :
						comments_template('', true);
					endif;
					?>
					<?php if (grosso_get_option('show_related_posts')): ?>
						<?php
						// Get random post from the same category as the current one
						$grosso_related_posts_args = array(
								'nopaging' => true,
								'post__not_in' => array($post->ID),
								'orderby' => 'rand',
								'post_type' => 'post',
								'post_status' => 'publish'
						);
						$grosso_get_terms_args = array(
								'orderby' => 'name',
								'order' => 'ASC',
								'fields' => 'slugs'
						);
						$grosso_categories = wp_get_post_terms($post->ID, 'category', $grosso_get_terms_args);
						if (!$grosso_categories instanceof WP_Error && !empty($grosso_categories)) {
							$grosso_related_posts_args['tax_query'] = array(array('taxonomy' => 'category', 'field' => 'slug', 'terms' => $grosso_categories));
						}

						wp_reset_postdata();

						//$grosso_related_posts = new WP_Query($grosso_related_posts_args);
						$grosso_is_latest_posts = true;
						// going off on my own here
						$grosso_temp_query = clone $wp_query;
						query_posts($grosso_related_posts_args);
						?>
						<?php if (have_posts()) : ?>
							<?php
							// owl carousel
							wp_localize_script('grosso-libs-config', 'grosso_owl_carousel', array(
									'include' => 'true'
							));
							?>
							<div class="grosso-related-blog-posts grosso_shortcode_latest_posts grosso_blog_masonry full_width">
								<h4><?php esc_html_e('Related posts', 'grosso') ?></h4>
								<div <?php if (grosso_get_option('owl_carousel')): ?> class="owl-carousel grosso-owl-carousel" <?php endif; ?>>
								<?php endif; ?>

								<?php while (have_posts()) : ?>
									<?php the_post(); ?>
							        <?php get_template_part('content', 'related-posts'); ?>
								<?php endwhile; ?>

								<?php if (have_posts()): ?>
								</div>
								<div class="clear"></div>
							</div>
						<?php endif; ?>
						<?php
						// going back to the main query
						$wp_query = clone $grosso_temp_query;
						?>
					<?php endif; ?>
				</div>

				<div class="clear"></div>
				<?php if (function_exists('grosso_share_links')): ?>
					<?php grosso_share_links(get_the_title(), get_permalink()); ?>
				<?php endif; ?>
			</div>

            <!-- Previous / Next links -->
			<?php if (grosso_get_option('show_prev_next')): ?>
				<?php echo grosso_post_nav(); ?>
			<?php endif; ?>
		</div>
		<!-- END OF CONTENT WRAPPER -->
	<?php endwhile; // end of the loop.    ?>
</div>
<?php
get_footer();
