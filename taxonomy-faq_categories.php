<?php
// The Default Page template file.

get_header();
global $wp_query;
$tax = $wp_query->get_queried_object();

$ID = get_page_by_path('faq')->ID;
// Get the grosso custom options
$grosso_page_options = get_post_custom($ID);
$grosso_current_post_type = get_post_type($ID);

$grosso_show_title_page = 'yes';
$grosso_show_breadcrumb = 'yes';
$grosso_featured_slider = 'none';
$grosso_rev_slider_before_header = 0;
$grosso_subtitle = '';
$grosso_show_title_background = 0;
$grosso_title_background_image = '';
$grosso_title_alignment = 'left_title';
$grosso_featured_flex_slider_imgs = array();

if (is_singular() && in_array($grosso_current_post_type, array('page', 'tribe_events'))) {

    if (isset($grosso_page_options['grosso_show_title_page']) && trim($grosso_page_options['grosso_show_title_page'][0]) != '') {
        $grosso_show_title_page = $grosso_page_options['grosso_show_title_page'][0];
    }

    if (isset($grosso_page_options['grosso_show_breadcrumb']) && trim($grosso_page_options['grosso_show_breadcrumb'][0]) != '') {
        $grosso_show_breadcrumb = $grosso_page_options['grosso_show_breadcrumb'][0];
    }

    if (isset($grosso_page_options['grosso_rev_slider']) && trim($grosso_page_options['grosso_rev_slider'][0]) != '') {
        $grosso_featured_slider = $grosso_page_options['grosso_rev_slider'][0];
    }

    if (isset($grosso_page_options['grosso_rev_slider_before_header']) && trim($grosso_page_options['grosso_rev_slider_before_header'][0]) != '') {
        $grosso_rev_slider_before_header = $grosso_page_options['grosso_rev_slider_before_header'][0];
    }

    if (isset($grosso_page_options['grosso_page_subtitle']) && trim($grosso_page_options['grosso_page_subtitle'][0]) != '') {
        $grosso_subtitle = $grosso_page_options['grosso_page_subtitle'][0];
    }

    if (isset($grosso_page_options['grosso_title_background_imgid']) && trim($grosso_page_options['grosso_title_background_imgid'][0]) != '') {
        $grosso_img = wp_get_attachment_image_src($grosso_page_options['grosso_title_background_imgid'][0], 'full');
        $grosso_title_background_image = $grosso_img[0];
    }

    if (isset($grosso_page_options['grosso_title_alignment']) && trim($grosso_page_options['grosso_title_alignment'][0]) != '') {
        $grosso_title_alignment = $grosso_page_options['grosso_title_alignment'][0];
    }

    $grosso_featured_flex_slider_imgs = grosso_get_more_featured_images($ID);
}

$grosso_sidebar_choice = apply_filters('grosso_has_sidebar', '');

if ($grosso_sidebar_choice != 'none') {
    $grosso_has_sidebar = is_active_sidebar($grosso_sidebar_choice);
} else {
    $grosso_has_sidebar = false;
}

$grosso_offcanvas_sidebar_choice = apply_filters('grosso_has_offcanvas_sidebar', '');

if ($grosso_offcanvas_sidebar_choice != 'none') {
    $grosso_has_offcanvas_sidebar = is_active_sidebar($grosso_offcanvas_sidebar_choice);
} else {
    $grosso_has_offcanvas_sidebar = false;
}

$grosso_sidebar_classes = array();
if ($grosso_has_sidebar) {
    $grosso_sidebar_classes[] = 'has-sidebar';
}
if ($grosso_has_offcanvas_sidebar) {
    $grosso_sidebar_classes[] = 'has-off-canvas-sidebar';
}
// Sidebar position
$grosso_sidebar_classes[] = apply_filters('grosso_left_sidebar_position_class', '');

// Title and events
$grosso_events_mode_and_title = grosso_get_current_events_display_mode_and_title($ID);
$grosso_title = $grosso_events_mode_and_title['title'];
$grosso_events_mode = $grosso_events_mode_and_title['display_mode'];

if (GROSSO_IS_EVENTS && in_array($grosso_events_mode, array(
    'MAIN_CALENDAR',
    'CALENDAR_CATEGORY',
    'MAIN_EVENTS',
    'CATEGORY_EVENTS',
    'SINGLE_EVENT_DAYS',
))
) {
    $grosso_img = wp_get_attachment_image_src(grosso_get_option('events_title_background_imgid'), 'full');
    if ($grosso_img) {
        $grosso_title_background_image = $grosso_img[0];
    }
    $grosso_subtitle = grosso_get_option('events_subtitle');
    $grosso_title_alignment = grosso_get_option('events_title_alignment');

}
// END title and events
?>
<?php if ($grosso_has_offcanvas_sidebar): ?>
	<?php get_sidebar('offcanvas');?>
<?php endif;?>
<div id="content" <?php if (!empty($grosso_sidebar_classes)) {
    echo 'class="' . esc_attr(implode(' ', $grosso_sidebar_classes)) . '"';
}
?> >
	<?php if ($grosso_show_title_page == 'yes' || $grosso_show_breadcrumb == 'yes'): ?>
		<div id="grosso_page_title" class="grosso_title_holder <?php echo esc_attr($grosso_title_alignment) ?> <?php if ($grosso_title_background_image): ?>title_has_image<?php endif;?>">
			<?php if ($grosso_title_background_image): ?>
				<div class="grosso-zoomable-background" style="background-image: url('<?php echo esc_url($grosso_title_background_image) ?>');"></div>
			<?php endif;?>
			<div class="inner fixed">
				<!-- BREADCRUMB -->
				<?php if ($grosso_show_breadcrumb == 'yes'): ?>
					<?php grosso_breadcrumb()?>
				<?php endif;?>
				<!-- END OF BREADCRUMB -->
				<!-- TITLE -->
				<?php if ($grosso_show_title_page == 'yes'): ?>
					<h1 class="heading-title">FAQ: <?php echo $tax->name; ?></h1>
					<?php if ($grosso_subtitle): ?>
						<h6><?php echo esc_html($grosso_subtitle) ?></h6>
					<?php endif;?>
				<?php endif;?>
				<!-- END OF TITLE -->
			</div>
		</div>
	<?php endif;?>
	<?php if ($grosso_featured_slider != 'none' && function_exists('putRevSlider') && !$grosso_rev_slider_before_header): ?>
		<!-- FEATURED REVOLUTION SLIDER -->
		<div class="slideshow">
			<div class="inner">
				<?php putRevSlider($grosso_featured_slider)?>
			</div>
		</div>
		<!-- END OF FEATURED REVOLUTION SLIDER -->
	<?php endif;?>
	<div class="inner">
		<!-- CONTENT WRAPPER -->
		<div id="main" class="fixed box box-common">
			<div class="content_holder">
                <?php get_template_part('content', 'faq-questions');?>
			</div>
			<!-- SIDEBARS -->
			<?php if ($grosso_has_sidebar): ?>
				<?php get_sidebar();?>
			<?php endif;?>
			<?php if ($grosso_has_offcanvas_sidebar): ?>
				<a class="sidebar-trigger" href="#"><?php echo esc_html__('show', 'grosso') ?></a>
			<?php endif;?>
			<!-- END OF SIDEBARS -->
			<div class="clear"></div>
			<?php if (function_exists('grosso_share_links')): ?>
				<?php grosso_share_links(get_the_title(), get_permalink());?>
			<?php endif;?>
		</div>
	</div>
</div>
<!-- END OF MAIN CONTENT -->

<?php
get_footer();
