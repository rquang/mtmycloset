<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes();?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type');?>; charset=<?php bloginfo('charset');?>" />
		<meta name="viewport" content="width=device-width, maximum-scale=1" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php esc_url(bloginfo('pingback_url'));?>" />
		<?php wp_head();?>
	</head>

	<body <?php body_class();?>>
		<?php if (grosso_get_option('show_preloader')): ?>
			<div class="mask">
				<div id="spinner"></div>
			</div>
		<?php endif;?>
		<?php if (grosso_get_option('add_to_cart_sound')): ?>
            <audio id="cart_add_sound" controls preload="auto" hidden="hidden">
                <source src="<?php echo GROSSO_IMAGES_PATH ?>cart_add.wav" type="audio/wav">
            </audio>
		<?php endif;?>
		<?php
global $grosso_is_blank;

// Set main menu as mobile if no mobile menu was set
$mobile_menu_id = 'primary';
if (has_nav_menu('mobile')) {
    $mobile_menu_id = "mobile";
}

if (!$grosso_is_blank) {
    // Top mobile menu
    $grosso_top_nav_mobile_args = array(
        'theme_location' => $mobile_menu_id,
        'container' => 'div',
        'container_id' => 'menu_mobile',
        'menu_id' => 'mobile-menu',
        'items_wrap' => grosso_build_mobile_menu_items_wrap(),
        'fallback_cb' => '',
        'walker' => new GrossoMobileMenuWalker(),
    );
    wp_nav_menu($grosso_top_nav_mobile_args);
}

// Are search or cart enabled or is account page
$grosso_is_search_or_cart_or_account = false;
if (grosso_get_option('show_searchform') || (GROSSO_IS_WOOCOMMERCE && grosso_get_option('show_shopping_cart')) || (GROSSO_IS_WOOCOMMERCE && get_option('woocommerce_myaccount_page_id'))) {
    $grosso_is_search_or_cart_or_account = true;
}

$grosso_general_layout = grosso_get_option('general_layout');
$grosso_specific_layout = get_post_meta(get_queried_object_id(), 'grosso_layout', true);

$grosso_meta_show_top_header = get_post_meta(get_queried_object_id(), 'grosso_top_header', true);
if (!$grosso_meta_show_top_header) {
    $grosso_meta_show_top_header = 'default';
}

$grosso_featured_slider = get_post_meta(get_queried_object_id(), 'grosso_rev_slider', true);
if (!$grosso_featured_slider) {
    $grosso_featured_slider = 'none';
}

$grosso_rev_slider_before_header = get_post_meta(get_queried_object_id(), 'grosso_rev_slider_before_header', true);
if (!$grosso_rev_slider_before_header) {
    $grosso_rev_slider_before_header = 0;
}
?>
		<?php if (grosso_get_option('show_searchform')): ?>
            <div id="search">
				<?php $grosso_search_options = grosso_get_option('search_options');?>
				<?php if (GROSSO_IS_WOOCOMMERCE && isset($grosso_search_options['only_products']) && $grosso_search_options['only_products']): ?>
					<?php get_product_search_form(true)?>
				<?php else: ?>
					<?php get_search_form();?>
				<?php endif;?>
            </div>
		<?php endif;?>
		<!-- MAIN WRAPPER -->
		<div id="container">
			<!-- If it is not a blank page template -->
			<?php if (!$grosso_is_blank): ?>
				<?php if (is_page() && $grosso_featured_slider != 'none' && function_exists('putRevSlider') && $grosso_rev_slider_before_header): ?>
					<!-- FEATURED REVOLUTION SLIDER -->
					<div class="grosso-intro slideshow">
						<div class="inner">
							<?php putRevSlider($grosso_featured_slider)?>
						</div>
					</div>
					<!-- END OF FEATURED REVOLUTION SLIDER -->
				<?php endif;?>
				<!-- Collapsible Pre-Header -->
				<?php if (grosso_get_option('enable_pre_header') && is_active_sidebar('pre_header_sidebar')): ?>
					<div id="pre_header"> <a href="#" class="toggler" id="toggle_switch" title="<?php esc_attr_e('Show/Hide', 'grosso')?>"><?php esc_html_e('Slide toggle', 'grosso')?></a>
						<div id="togglerone" class="inner">
							<!-- Pre-Header widget area -->
							<?php dynamic_sidebar('pre_header_sidebar')?>
							<div class="clear"></div>
						</div>
					</div>
				<?php endif;?>
				<!-- END Collapsible Pre-Header -->
				<!-- HEADER -->
				<?php
$grosso_should_show_top_header = false;
if (grosso_get_option('enable_top_header') && $grosso_meta_show_top_header == 'default' || $grosso_meta_show_top_header == 'show') {
    $grosso_should_show_top_header = true;
}

$grosso_theme_logo_img = grosso_get_option('theme_logo');
$grosso_transparent_theme_logo_img = grosso_get_option('transparent_theme_logo');
$grosso_mobile_logo_img = grosso_get_option('mobile_theme_logo');

// If there is no secondary logo add 'persistent_logo' class to the main logo
$grosso_persistent_logo_class = $grosso_transparent_theme_logo_img ? '' : 'persistent_logo';

if (!$grosso_theme_logo_img && !$grosso_transparent_theme_logo_img && (get_bloginfo('name') || get_bloginfo('description'))) {
    $grosso_is_text_logo = true;
} else {
    $grosso_is_text_logo = false;
}

$grosso_header_classes = array();
if ($grosso_should_show_top_header) {
    $grosso_header_classes[] = 'grosso-has-header-top';
}
if ($grosso_is_text_logo) {
    $grosso_header_classes[] = 'grosso_text_logo';
}
?>
				<div id="header" <?php if (!empty($grosso_header_classes)): ?>class="<?php echo esc_attr(implode(' ', $grosso_header_classes)) ?>" <?php endif;?> >
					<?php if ($grosso_should_show_top_header): ?>
						<div id="header_top" class="fixed">
							<div class="inner<?php if (has_nav_menu("secondary")) {
    echo " has-top-menu";
}
?>">
								<?php if (function_exists('icl_get_languages')): ?>
									<div id="language">
										<?php grosso_language_selector_flags();?>
									</div>
								<?php endif;?>
								<!--	Social profiles in header-->
								<?php if (grosso_get_option('social_in_header')): ?>
									<?php get_template_part('partials/social-profiles');?>
								<?php endif;?>
								<?php if (grosso_get_option('top_bar_message') || grosso_get_option('top_bar_message_phone') || grosso_get_option('top_bar_message_email')): ?>
									<div class="grosso-top-bar-message">
										<?php echo esc_html(grosso_get_option('top_bar_message')) ?>
										<?php if (grosso_get_option('top_bar_message_email')): ?>
											<span class="grosso-top-bar-mail">
												<?php if (grosso_get_option('top_bar_message_email_link')): ?><a href="mailto:<?php echo esc_html(grosso_get_option('top_bar_message_email')) ?>"><?php endif;?>
													<?php echo esc_html(grosso_get_option('top_bar_message_email')) ?>
												<?php if (grosso_get_option('top_bar_message_email_link')): ?></a><?php endif;?>
											</span>
										<?php endif;?>
										<?php if (grosso_get_option('top_bar_message_phone')): ?>
											<span class="grosso-top-bar-phone">
												<?php if (grosso_get_option('top_bar_message_phone_link')): ?><a href="tel:<?php echo preg_replace("/[^0-9+-]/", "", esc_html(grosso_get_option('top_bar_message_phone'))) ?>"><?php endif;?>
													<?php echo esc_html(grosso_get_option('top_bar_message_phone')) ?>
												<?php if (grosso_get_option('top_bar_message_phone_link')): ?></a><?php endif;?>
											</span>
										<?php endif;?>
									</div>
								<?php endif;?>
								<?php
/* Secondary menu */
$grosso_side_nav_args = array(
    'theme_location' => 'secondary',
    'container' => 'div',
    'container_id' => 'menu',
    'menu_class' => '',
    'menu_id' => 'topnav2',
    'fallback_cb' => '',
);
wp_nav_menu($grosso_side_nav_args);
?>
							</div>
						</div>
					<?php endif;?>

					<div class="inner main_menu_holder fixed<?php if (has_nav_menu('primary')) {
    echo ' has-main-menu';
}
?>">
						<div <?php if ($grosso_is_text_logo) {
    echo 'class="grosso_text_logo"';
}
?> id="logo">
							<a href="<?php echo esc_url(grosso_wpml_get_home_url()); ?>"  title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
								<?php
// Main logo
if ($grosso_theme_logo_img) {
    echo wp_get_attachment_image($grosso_theme_logo_img, 'full', false, array('class' => esc_attr($grosso_persistent_logo_class)));
}

// Secondary logo
if ($grosso_transparent_theme_logo_img) {
    echo wp_get_attachment_image($grosso_transparent_theme_logo_img, 'full', false, array('class' => 'transparent_logo'));
}

// Mobile logo
if ($grosso_mobile_logo_img) {
    echo wp_get_attachment_image($grosso_mobile_logo_img, 'full', false, array('class' => 'grosso_mobile_logo'));
}
?>
								<?php if ($grosso_is_text_logo): ?>
									<span class="grosso-logo-title"><?php bloginfo('name')?></span>
									<span class="grosso-logo-subtitle"><?php bloginfo('description')?></span>
								<?php endif;?>
							</a>
						</div>
						<a class="mob-menu-toggle" href="#"><i class="fa fa-bars"></i></a>

						<?php if ($grosso_is_search_or_cart_or_account): ?>
							<div class="grosso-search-cart-holder">
								<?php if (grosso_get_option('show_searchform')): ?>
                                    <div class="grosso-search-trigger">
                                        <a href="#" title="<?php echo esc_attr__('Search', 'grosso') ?>"><i class="fa fa-search"></i></a>
                                    </div>
								<?php endif;?>

								<!-- SHOPPING CART -->
								<?php if (GROSSO_IS_WOOCOMMERCE && grosso_get_option('show_shopping_cart')): ?>
									<ul id="cart-module" class="site-header-cart">
										<?php grosso_cart_link();?>
										<li>
											<?php the_widget('WC_Widget_Cart', 'title=');?>
										</li>
									</ul>
								<?php endif;?>
								<!-- END OF SHOPPING CART -->

								<?php if (grosso_should_show_wishlist_icon()): ?>
									<div class="grosso-wishlist-counter">
										<a href="<?php echo esc_url(YITH_WCWL()->get_wishlist_url()); ?>" title="<?php echo esc_attr__('Wishlist', 'grosso') ?>">
											<i class="fa fa-heart"></i>
											<span class="grosso-wish-number"><?php echo esc_html(YITH_WCWL()->count_products()); ?></span>
										</a>
									</div>
								<?php endif;?>

								<?php global $current_user;?>

								<?php $grosso_has_content_woocommerce_my_account = false;?>
								<?php if (isset($post->post_content) && has_shortcode($post->post_content, 'woocommerce_my_account')): ?>
									<?php $grosso_has_content_woocommerce_my_account = true;?>
								<?php endif;?>

								<?php if (grosso_should_show_account_icon() && (is_user_logged_in() || (!is_user_logged_in() && !$grosso_has_content_woocommerce_my_account))): ?>
									<?php wp_get_current_user();?>
									<?php
$grosso_account_holder_classes = array();
if (is_user_logged_in()) {
    $grosso_account_holder_classes[] = 'grosso-user-is-logged';
} else {
    $grosso_account_holder_classes[] = 'grosso-user-not-logged';
}
?>
                                    <div id="grosso-account-holder" <?php if (count($grosso_account_holder_classes)) {
    echo 'class="' . implode(' ', $grosso_account_holder_classes) . '"';
}
?> >
                                        <a href="<?php echo esc_url(get_permalink(get_option('woocommerce_myaccount_page_id'))); ?>" title="<?php esc_attr_e('My Account', 'grosso');?>">
                                            <i class="fa fa-user"></i>
                                        </a>
                                        <div class="grosso-header-account-link-holder">
		                                    <?php if (is_user_logged_in()): ?>
                                                <ul>
                                                    <li>
                                                        <span class="grosso-header-user-data">
                                                            <?php echo get_avatar($current_user->ID, 60); ?>
                                                            <small><?php echo esc_html($current_user->display_name); ?></small>
                                                        </span>
                                                    </li>
				                                    <?php if (GROSSO_IS_WC_MARKETPLACE && is_user_wcmp_vendor($current_user)): ?>
                                                        <li class="grosso-header-account-wcmp-dash">
	                                                        <?php $grosso_wcmp_dashboard_page_link = wcmp_vendor_dashboard_page_id() ? get_permalink(wcmp_vendor_dashboard_page_id()) : '#';?>
	                                                        <?php echo apply_filters('wcmp_vendor_goto_dashboard', '<a href="' . esc_url($grosso_wcmp_dashboard_page_link) . '">' . esc_html__('Vendor Dashboard', 'grosso') . '</a>'); ?>
                                                        </li>
				                                    <?php elseif (GROSSO_IS_WC_VENDORS_PRO && WCV_Vendors::is_vendor($current_user->ID)): ?>
                                                        <li class="grosso-header-account-vcvendors-pro-dash">
						                                    <?php $grosso_wcv_pro_dashboard_page = WCVendors_Pro::get_option('dashboard_page_id');?>
						                                    <?php if ($grosso_wcv_pro_dashboard_page): ?>
                                                                <a href="<?php echo esc_url(get_permalink($grosso_wcv_pro_dashboard_page)); ?>"><?php echo esc_html__('Vendor Dashboard', 'grosso'); ?></a>
						                                    <?php endif;?>
                                                        </li>
				                                    <?php elseif (GROSSO_IS_WC_VENDORS && WCV_Vendors::is_vendor($current_user->ID)): ?>
                                                        <li class="grosso-header-account-vcvendors-dash">
						                                    <?php $grosso_wcv_free_dashboard_page = WC_Vendors::$pv_options->get_option('vendor_dashboard_page');?>
						                                    <?php if ($grosso_wcv_free_dashboard_page): ?>
                                                                <a href="<?php echo esc_url(get_permalink($grosso_wcv_free_dashboard_page)); ?>"><?php echo esc_html__('Vendor Dashboard', 'grosso'); ?></a>
						                                    <?php endif;?>
                                                        </li>
				                                    <?php endif;?>
				                                    <?php foreach (wc_get_account_menu_items() as $endpoint => $label): ?>
                                                        <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?>">
                                                            <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo esc_html($label); ?></a>
                                                        </li>
				                                    <?php endforeach;?>
                                                </ul>
		                                    <?php elseif (!$grosso_has_content_woocommerce_my_account): ?>
												<?php echo do_shortcode('[woocommerce_my_account]'); ?>
												<?php echo do_shortcode('[woocommerce_social_login_buttons return_url="https://mtmycloset.com/account"]'); ?>
		                                    <?php endif;?>
                                        </div>
                                    </div>
								<?php endif;?>

							</div>
						<?php endif;?>
						<?php
// Top menu
$grosso_top_menu_container_class = 'menu-main-menu-container';

$grosso_top_nav_args = array(
    'theme_location' => 'primary',
    'container' => 'div',
    'container_id' => 'main-menu',
    'container_class' => $grosso_top_menu_container_class,
    'menu_id' => 'main_nav',
    'fallback_cb' => '',
    'walker' => new GrossoFrontWalker(),
);
wp_nav_menu($grosso_top_nav_args);
?>
					</div>
				</div>
				<!-- END OF HEADER -->
			<?php endif;?>
