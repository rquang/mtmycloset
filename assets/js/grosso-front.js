(function ($) {
    "use strict";

    /**
     * Use this file to override javascript functions
     * defined in the Grosso parent theme grosso-front.js file
     *
     * For example:
     *
     *  window.grossoStickyHeaderInit = function (){
     *    // Your code here will be executed instead of
     *    // those from the parent theme's function
     * }
     *
     */

})(window.jQuery);