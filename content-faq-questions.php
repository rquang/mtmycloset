<?php
global $wp_query;
$tax = $wp_query->get_queried_object();
?>

<div class="row mb-5">
    <div id="accordion" class="col-12">
        <div class="row">
            <?php
$args = array(
    'post_type' => 'faq',
    'orderby' => 'publish_date',
    'order' => 'ASC',
    'hide_empty' => 1,
    'faq_categories' => $tax->slug,
);
$loop = new WP_Query($args);
write_log($loop);
$count = 0;
if ($loop->have_posts()):
    foreach ($loop->get_posts() as $p):
        $ID = $p->ID;
        ++$count;
        $question = 'question-' . $count;
        $answer = 'answer-' . $count;
        ?>
						                    <div class="col-12 col-md-6 mb-2">
						                        <div class="card">
						                            <div class="card-header" id="<?php echo $question ?>">
						                                <h5 class="mb-0">
						                                    <button class="btn text-uppercase w-100 text-left" data-toggle="collapse" data-target="#<?php echo $answer ?>" aria-expanded="true"
						                                        aria-controls="<?php echo $answer ?>">
						                                        <?php echo get_the_title($ID); ?>
						                                    </button>
						                                </h5>
						                            </div>

						                            <div id="<?php echo $answer ?>" class="collapse" aria-labelledby="<?php echo $question ?>" data-parent="#accordion">
						                                <div class="card-body">
						                                    <?php the_field('answer', $ID);?>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                    <?php
    endforeach;
endif;
?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <a href="/faq" class="btn primary-btn">Back To All FAQ</a>
    </div>
</div>