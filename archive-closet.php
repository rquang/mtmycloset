<?php
// The Archive template file.

get_header();

$grosso_title_background_image = grosso_get_option( 'blog_title_background_imgid' );

if ( $grosso_title_background_image ) {
	$grosso_img                    = wp_get_attachment_image_src( $grosso_title_background_image, 'full' );
	$grosso_title_background_image = $grosso_img[0];
}

// Blog style
$grosso_general_blog_style = grosso_get_option('general_blog_style');
switch ($grosso_general_blog_style) {
    case 'grosso_blog_masonry':
		// load Isotope
		wp_enqueue_script('isotope');
		// Isotope settings
		wp_localize_script('grosso-libs-config', 'grosso_masonry_settings', array(
			'include' => 'true'
		));
		break;
}
?>
    <div id="content" <?php if ( ! empty( $grosso_sidebar_classes ) ) {
		echo 'class="' . esc_attr( implode( ' ', $grosso_sidebar_classes ) ) . '"';
	} ?> >
        <div id="grosso_page_title"
             class="grosso_title_holder <?php if ( $grosso_title_background_image ): ?>title_has_image<?php endif; ?>">
			<?php if ( $grosso_title_background_image ): ?>
                <div class="grosso-zoomable-background"
                     style="background-image: url('<?php echo esc_url( $grosso_title_background_image ) ?>');"></div><?php endif; ?>
            <div class="inner fixed">
                <!-- BREADCRUMB -->
				<?php grosso_breadcrumb() ?>
                <!-- END OF BREADCRUMB -->
                <h1 class="heading-title">
					Browse Closets
                </h1>
            </div>
        </div>
        <div class="inner">
            <!-- CONTENT WRAPPER -->
            <div id="main" class="fixed box box-common">

                <div class="content_holder<?php if($grosso_general_blog_style) echo ' '.esc_attr($grosso_general_blog_style); ?>">
					<?php if ( have_posts() ): ?>

						<?php echo term_description(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

                            <!-- BLOG POST -->
							<?php get_template_part( 'templates/closet/single', get_post_format() ); ?>
							<?php get_template_part( 'templates/closet/items', get_post_format() ); ?>
                            <!-- END OF BLOG POST -->

						<?php endwhile; ?>
					<?php else: ?>
                        <p><?php esc_html_e( 'No posts were found. Sorry!', 'grosso' ); ?></p>
					<?php endif; ?>

                    <!-- PAGINATION -->
                    <div class="box box-common">
						<?php
						if ( function_exists( 'grosso_pagination' ) ) : grosso_pagination();
						else :
							?>

                            <div class="navigation group">
                                <div class="alignleft"><?php next_posts_link( __( 'Next &raquo;', 'grosso' ) ) ?></div>
                                <div class="alignright"><?php previous_posts_link( __( '&laquo; Back', 'grosso' ) ) ?></div>
                            </div>

						<?php endif; ?>
                    </div>
                    <!-- END OF PAGINATION -->
                </div>
                <div class="clear"></div>

            </div>
            <!-- END OF CONTENT WRAPPER -->
        </div>
    </div>
<?php
get_footer();
